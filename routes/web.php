<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();

Route::get('/', 'IndexController@index');
Route::get('project', 'PortfolioController@list');
Route::get('testimonial', 'PortfolioController@testimonial');
Route::get('detail-project/{id}', 'PortfolioController@view');
Route::get('blog', 'IndexController@listBlog');
Route::get('view-blog/{id}', 'IndexController@viewBlog');

//Preview
Route::get('preview-file/{path}/{file}', 'HomeController@FileView');



Route::get('admin-papiliotech', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login-post', 'Auth\LoginController@loginPost');
Route::get('/logout', 'Auth\LoginController@logout');

Route::post('send-messages', 'MessagesController@store');

Route::group(['middleware' => ['checkauth']], function () {

Route::group(['middleware' => ['checkAdmin']], function () {
Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/home', 'HomeController@index')->name('home');

//Menu
Route::get('setting-web', 'SettingAppsController@index')->name('setting-web');
Route::post('setting-web', 'SettingAppsController@store');

//Menu
Route::get('data-menu', 'MenusController@index')->name('data-menu');
Route::get('create-menu', 'MenusController@create');
Route::post('create-menu', 'MenusController@store');
Route::get('edit-menu/{id}', 'MenusController@edit');
Route::post('edit-menu/{id}', 'MenusController@update');

//Category
Route::get('data-category', 'CategoryController@index')->name('data-category');
Route::get('create-category', 'CategoryController@create');
Route::post('create-category', 'CategoryController@store');
Route::get('edit-category/{id}', 'CategoryController@edit');
Route::post('edit-category/{id}', 'CategoryController@update');
Route::get('delete-category/{id}', 'CategoryController@destroy');

//Founder
Route::get('data-founder', 'FounderController@index')->name('data-category');
Route::get('create-founder', 'FounderController@create');
Route::post('create-founder', 'FounderController@store');
Route::get('edit-founder/{id}', 'FounderController@edit');
Route::post('edit-founder/{id}', 'FounderController@update');
Route::get('delete-founder/{id}', 'FounderController@destroy');

//Services
Route::get('data-services', 'ServicesController@index')->name('data-services');
Route::get('create-services', 'ServicesController@create');
Route::post('create-services', 'ServicesController@store');
Route::get('edit-services/{id}', 'ServicesController@edit');
Route::post('edit-services/{id}', 'ServicesController@update');

//Clients
Route::get('data-clients', 'ClientsController@index')->name('data-clients');
Route::get('create-clients', 'ClientsController@create');
Route::post('create-clients', 'ClientsController@store');
Route::get('edit-clients/{id}', 'ClientsController@edit');
Route::post('edit-clients/{id}', 'ClientsController@update');

//Messages
Route::get('data-messages', 'MessagesController@index')->name('data-messages');
Route::get('create-messages', 'MessagesController@create');
Route::post('create-messages', 'MessagesController@store');
Route::get('view-messages/{id}', 'MessagesController@view');

//Testiomonials
Route::get('data-testimonials', 'TestimonialsController@index')->name('data-testimonials');
Route::get('create-testimonials', 'TestimonialsController@create');
Route::post('create-testimonials', 'TestimonialsController@store');
Route::get('edit-testimonials/{id}', 'TestimonialsController@edit');
Route::post('edit-testimonials/{id}', 'TestimonialsController@update');
Route::get('delete-testimonials/{id}', 'TestimonialsController@destroy');

//Blog
Route::get('data-blog', 'BlogController@index')->name('data-testimonials');
Route::get('create-blog', 'BlogController@create');
Route::post('create-blog', 'BlogController@store');
Route::get('edit-blog/{id}', 'BlogController@edit');
Route::post('edit-blog/{id}', 'BlogController@update');
Route::get('delete-blog/{id}', 'BlogController@destroy');

//Administrator
Route::get('data-administrator', 'AdministratorController@index')->name('data-administrator');
Route::get('create-administrator', 'AdministratorController@create');
Route::post('create-administrator', 'AdministratorController@store');
Route::get('edit-administrator/{id}', 'AdministratorController@edit');
Route::post('edit-administrator/{id}', 'AdministratorController@update');
Route::get('edit-password-administrator/{id}', 'AdministratorController@editPassword');
Route::post('edit-password-administrator/{id}', 'AdministratorController@updatePassword');
Route::get('delete-administrator/{id}', 'AdministratorController@destroy');

//Projects
Route::get('data-projects', 'ProjectsController@index')->name('data-projects');
Route::get('create-projects', 'ProjectsController@create');
Route::post('create-projects', 'ProjectsController@store');
Route::get('edit-projects/{id}', 'ProjectsController@edit');
Route::post('edit-projects/{id}', 'ProjectsController@update');
Route::get('delete-projects/{id}', 'ProjectsController@destroy');

//Change Status
Route::get('change-status/{id}/{sesi}','HomeController@changeStatus');

});

});
