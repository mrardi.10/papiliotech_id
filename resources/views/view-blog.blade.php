@extends('layouts.app_frontend')

@section('content')
  <!-- navbar -->
  <nav class="navbar fixed-top navbar-secondary navbar-expand-lg navbar-light">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('assets/frontend/images/PAPILIOtech-white.png') }}" class="img-fluid" alt="papiliotech"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fas fa-align-justify"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('testimonial') }}">Testimonials</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('blog') }}">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('project') }}">Portfolio</a>
                </li>
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown link
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li> -->
            </ul>
        </div>
    </div>
</nav>
</header>

<main id="post-detail">
    <section id="article">
        <div class="container-fluid">
            <div class="row head-blog"></div>
        </div>
        <div class="container">
            <div class="row content-blog justify-content-center">
                <div class="col-12 col-lg-10 col-xl-12">
                    <article>
                        <header class="text-center">
                            <h1>{{ $data->judul }}</h1>
                            <div class="credit">
                                @if ($created->avatar != "")
                                <div class="ava"><img src="{{ url('preview-file/ava-users/'.$created->avatar) }}" class="img-fluid" alt=""></div>
                                @else
                                <div class="ava"><img src="{{ asset('assets/backend/plugins/dropify/not_found.png') }}" class="img-fluid" alt=""></div>
                                @endif

                                <small class="text-uppercase"> BY {{ $created->name }} | <span> @foreach ($categories as $item)
                                    {{ $item->categories }}
                                @endforeach </span> | {{ Helper::bulan_tanggal_en($data->tanggal) }}, {{ date('Y',strtotime($data->tanggal)) }}</small>
                            </div>
                            <div class="share-article">
                                <ul class="nav justify-content-center">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ $apps->fb }}"><img src="{{ asset('assets/frontend/images/ic_fb_circle.png') }}" alt="" title="Share to Facebook"></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ $apps->twitter }}"><img src="{{ asset('assets/frontend/images/ic_tw_circle.png') }}" alt="" title="Share to Twitter"></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ $apps->ig }}"><img src="{{ asset('assets/frontend/images/ic_in_crcle.png') }}" alt="" title="Share to Instagram"></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ $apps->linked_in }}"><img src="{{ asset('assets/frontend/images/ic_tl_circle.png') }}" alt="" title="Share to LinkedIn"></a>
                                    </li>
                                </ul>
                            </div>
                        </header>
                        <div class="card">
                            <div class="card-body">
                                <img src="{{ url('preview-file/foto-blog/'.$data->gambar) }}" alt="" title="">
                            </div>
                        </div>
                        <div class="content">
                           @php
                           echo $data->isi;
                           @endphp
                           @if ($blog_most != "")
                            @if ($blog_most->id != $data->id)
                            <blockquote class="blockquote">
                                <p class="mb-0">Read this : <a href="{{ url('view-blog/'.$blog_most->id) }}"> {{ $blog_most->judul }}</a></p>
                            </blockquote>
                            @endif
                           @elseif($blog_readthis != "")
                           @if ($blog_readthis->id != $data->id)
                            <blockquote class="blockquote">
                                <p class="mb-0">Read this : <a href="{{ url('view-blog/'.$blog_readthis->id) }}"> {{ $blog_readthis->judul }}</a></p>
                            </blockquote>
                            @endif
                           @endif

                            {{-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam, suscipit accusantium, sit incidunt autem facilis vel impedit dolor pariatur consectetur ullam hic illo, voluptas ab dolorum alias magnam mollitia omnis optio qui perspiciatis neque eius eum sequi. Doloremque id iste ipsam qui nobis distinctio sint deserunt quisquam esse porro eaque omnis minima atque labore, et autem deleniti fugit voluptatum magnam ratione cumque sequi fugiat libero? Deserunt numquam quos sint, distinctio omnis necessitatibus neque! Culpa possimus expedita itaque adipisci numquam quisquam, vel eveniet quia, cumque quas autem labore necessitatibus eius fugiat in earum quos nobis, consequatur dolorem ex illo quo! Quidem minus natus excepturi exercitationem dolor tenetur quasi possimus. Voluptatem non, nostrum corrupti labore, culpa nemo laudantium mollitia minima numquam dicta officia, soluta similique recusandae reprehenderit doloremque possimus natus doloribus adipisci ducimus ea eius! Qui itaque quidem dolore mollitia et consequuntur quam fugiat esse, quisquam neque hic laudantium repellat, unde quasi doloremque quia! Consequatur molestias aut deleniti? Ea facere sequi, mollitia impedit laboriosam explicabo nesciunt corporis? Maxime officiis, veritatis ratione totam in corrupti eius labore doloremque dolorum quo dolor! Earum expedita esse voluptates ducimus soluta laborum accellat, unde quasi doloremque quia! Consequatur molestias aut deleniti? Ea facere sequi, mollitia impedit laboriosam explicabo nesciunt corporis? Maxime officiis, veritatis ratione totam in corrupti eius labore doloremque dolorum quo dolor! Earum expedita esse voluptates ducimus soluta laborum accellat, unde quasi doloremque quia! Consequatur molestias aut deleniti? Ea facere sequi, mollitia impedit laboriosam explicabo nesciunt corporis? Maxime officiis, veritatis ratione totam in corrupti eius labore doloremque dolorum quo dolor! Earum expedita esse voluptates ducimus soluta laborum accusamus. Sunt quos debitis ab magni voluptate non vel minus numquam explicabo voluptatum? Libero, blanditiis.</p>
                            <h3>Subtitle</h3>
                            <ul>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus, harum!</li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus, harum!</li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus, harum!</li>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus, harum!</li>
                            </ul> --}}

                        </div>
                        <div class="tags">
                            <p><i class="fas fa-tags"></i> Tags: <span>  @php
                                $n_catg = count($tags);
                                $n_c = 1;
                                @endphp
                                @foreach ($tags as $item)
                                @if($data->id == $item->id_blog)
                                    {{$item->categories}}
                                    @if($n_c != $n_catg)
                                    ,
                                     @elseif($n_c == $n_catg)

                                     @endif
                                     @php
                                         $n_c++;
                                     @endphp
                                @endif
                                @endforeach </span></p>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <section id="related-article">
        <hr>
        <div class="container">
            <div class="row related-article justify-content-center">
                <div class="col-12">
                    <header><h3>Related Article</h3></header>
                </div>
                @if (count($blog_related) > 0)
                @foreach ($blog_related as $item)

                <div class="col-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="card">
                        <img src="{{ url('preview-file/foto-blog/'.$item->gambar) }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4><a href="{{ url('view-blog/'.$item->id) }}"> {{ $item->judul }}</a></h4>
                            <p class="card-text">@php echo $item->preview; @endphp</p>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"></li>
                            <li class="list-group-item d-flex justify-content-between">
                                <span class="category">
                                    {{ $item->categories }}
                                    {{-- @foreach ($categories as $item)
                                        {{ $item->categories }}
                                    @endforeach --}}
                                </span>
                                <span class="post-date">{{ Helper::bulan_tanggal_en($item->tanggal) }}, {{ date('Y',strtotime($item->tanggal)) }}</span>
                            </li>
                            <li class="list-group-item credit">
                                <div class="ava"><img src="{{ url('preview-file/ava-users/'.$item->avatar) }}" class="img-fluid" alt=""></div>
                                <span class="text-uppercase"> BY <b>{{ $item->name }}</b> </span>
                            </li>
                        </ul>
                    </div>
                </div>

                @endforeach
                @else
                <div class="col-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="card">
                        <img src="{{ asset('assets/frontend/images/blog/drew-beamer-Se7vVKzYxTI-unsplash.jpg') }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4><a href="#"> Lorem ipsum dolor sit amet consectetur.</a></h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the
                                bulk of the card's content.</p>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"></li>
                            <li class="list-group-item d-flex justify-content-between">
                                <span class="category">Design</span>
                                <span class="post-date">May 02, 2020</span>
                            </li>
                            <li class="list-group-item credit">
                                <div class="ava"><img src="{{ asset('assets/frontend/images/tedy.jp') }}g" class="img-fluid" alt=""></div>
                                <span> BY <b>TEDY HIDAYAT</b></span>
                            </li>
                        </ul>
                    </div>
                </div>
                @endif
            </div>
            <div class="row coments-disqus">
                <div class="col-12">

                </div>
            </div>
        </div>
    </section>
</main>

@endsection

