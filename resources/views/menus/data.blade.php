@extends('layouts.app_backend')

@section('title')
Data Menu
@stop

@section('title_page')
Data Menu
@stop

@section('css_after')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('assets/backend/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('assets/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  {{-- <link rel="stylesheet" href="{{ asset('assets/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}"> --}}
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/datatables-fixedheader/css/fixedHeader.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@stop

@section('content')
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-12 col-xs-12">
<div class="box">
    <div class="box-header">
        <h3 class="card-title">Silahkan mencari berdasarkan filter yang tersedia</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row mx-0 mb-4">
            <div class="col-md-6">
              <div class="row">
                  <div class="col-8 col-sm-8 col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <i class="fa fa-search"></i>
                        </span>
                        <input type="search" id="searchTable" class="form-control " placeholder="Cari disini..">
                      </div>
                  </div>
                  <div class="col-4 col-sm-4 col-md-3">
                    <div class="input-group ">
                        <select id="showRowTable" class="form-control">
                          <option value="5">5</option>
                          <option value="10">10</option>
                          <option value="20">20</option>
                          <option value="50">50</option>
                          <option value="100">100</option>
                          <option value="250">250</option>
                          <option value="500">500</option>
                        </select>
                      </div>
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <a href="{{ url('create-menu')}}">
                <button class="btn bg-primary pull-right">Tambah</button>
              </a>
            </div>
          </div>
      <table id="dataMenu" class="table table-striped table-hover table-ver-1">
        <thead>
        <tr>
          <th>#</th>
          <th>Menu</th>
          <th>Link</th>
          <th>Status</th>
          <th>Urutan</th>
          <th>Action</th>
        </tr>
        </thead>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
      </div>
    </div>

@endsection

@section('js_after')
    <!-- Select2 -->
<script src="{{ asset('assets/backend/plugins/select2/js/select2.full.min.js')}}"></script>

<!-- DataTables -->
<script src="{{ asset('assets/backend/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/datatables/typeahead/bootstrap4-typeahead.js')}}"></script>

<script type="text/javascript">

    $(document).ready(function(){

    // $('.select-filter-jurusan').select2({
    //     theme: 'bootstrap4'
    // });

    fetch_data();
    function fetch_data(filter_jurusan = '')
    {
      var dataSrc = [];

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      var dataTable = $('#dataMenu').DataTable({
          "autoWidth": false,
          "responsive": true,
          "pageLength": 20,
          language: {
              processing: '<div class="load-ajax text-center bg-transparent"><i class="fa fa-spinner fa-spin fa-2x"></i><span class="font-w600 ml-10 pb-50">Processing....</span></div>',
          paginate: {
            previous: 'Sebelumnya',
            next:     'Selanjutnya'
          },
          aria: {
            paginate: {
              previous: 'Sebelumnya',
              next:     'Selanjutnya'
            }
          },
          emptyTable: 'Tidak terdapat data dalam Tabel'
        },
          processing: true,
          serverSide: true,
          ajax:{url: "{{ url('data-menu') }}",

              data: {filter_jurusan:filter_jurusan}
          },
          columns: [
                {data: 'rownum', name: 'rownum', searchable: false},
                {data: 'nama', name: 'nama'},
                {data: 'link', name: 'link'},
                {data: 'status', name: 'status'},
                {data: 'urutan', name: 'urutan'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            'initComplete': function(){
        var api = this.api();

            // Populate a dataset for autocomplete functionality
            // using data from first, second and third columns
            api.cells('tr', [1, 2,3,4]).every(function(){
              // Get cell data as plain text
              var data = $('<div>').html(this.data()).text();
              if(dataSrc.indexOf(data) === -1){ dataSrc.push(data); }
            });

            // Sort dataset alphabetically
            dataSrc.sort();


            // Initialize Typeahead plug-in
            $('#searchTable').typeahead({
              source: dataSrc,
              afterSelect: function(value){
              api.search(value).draw();
            }

          });
          }
      });
     }

     $('#filter_jurusan').change(function(){
      var val = $('#filter_jurusan').val();
      $('#dataMenu').DataTable().clear().destroy();
      fetch_data(val);
     });

     $('#searchTable').on('keyup',function(){
        $('#dataMenu').DataTable().search($(this).val()).draw();
      });

    $('#showRowTable').val($('#dataMenu').DataTable().page.len());

    $('#showRowTable').change( function() {
        $('#dataMenu').DataTable().page.len( $(this).val() ).draw();
    });

    });


      </script>
@endsection
