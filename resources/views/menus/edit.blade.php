@extends('layouts.app_backend')

@section('title')
Ubah Menu
@stop

@section('title_page')
Ubah Menu
@stop

@section('css_after')
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

@stop

@section('content')
<section class="content">
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Data Menu</h3><br>
                <p>Pastikan yang merupakan tanda (*) diisi dengan benar</p>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="" method="post" id="form_master">
              	@csrf
                <div class="box-body">

                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                  			  <label for="nama">Nama Menu <span class="wajib">*</span></label>
							<input type="text" name="nama" class="form-control" id="nama" value="{{ $data->nama }}">
                  			  <span class="err" id="errNama"></span>
                            </div>
                            <div class="form-group">
                  			  <label for="urutan">Urutan <span class="wajib">*</span></label>
							<input type="number" min="1" name="urutan" class="form-control" id="urutan" value="{{ $data->urutan }}">
                  			  <span class="err" id="errUrutan"></span>
							</div>
							<div class="form-group">
								<label for="status">Status <span class="wajib">*</span></label>
								<select class="form-control" id="status" name="status">
									<option value="" {{ $data->status == '' ? 'selected' : '' }}>-----</option>
									<option value="1" {{ $data->status == 1 ? 'selected' : '' }}>Aktif</option>
									<option value="0" {{ $data->status == 0 ? 'selected' : '' }}>Tidak Aktif</option>
								</select>
								<span class="err" id="errStatus"></span>
							  </div>
						</div>
                  	</div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="button" class="btn bg-primary" id="btn_submit">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
</section>
@stop

@section('js_after')

<script type="text/javascript">

	$(document).ready(function(){

	});

    var valid_nama = new Boolean(false);
    var valid_urutan = new Boolean(false);
    var valid_status = new Boolean(false);

	$('#nama').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_nama);
			$('#errNama').html("Nama Menu tidak boleh Kosong");

		}else{
			$('#errNama').html("");
			setToTrue(valid_nama);
		}
	});

    $('#urutan').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_nama);
			$('#errUrutan').html("Urutan tidak boleh Kosong");

		}else{
			$('#errUrutan').html("");
			setToTrue(valid_nama);
		}
	});


	$('#status').on('change',function(){
		if ($(this).val() == "") {
			$('#errStatus').html("Status tidak boleh Kosong");
			setToFalse(valid_status);

		}else{
			$('#errStatus').html("");
			setToTrue(valid_status);
		}
	});

	$('#btn_submit').on('click',function(e){


		if ($('#status').val().length == 0) {
			setToFalse(valid_status);
		}else{
			setToTrue(valid_status);
		}

        if ($('#urutan').val().length == 0) {
			setToFalse(valid_urutan);
		}else{
			setToTrue(valid_urutan);
		}

		if ($('#nama').val().length == 0) {
			setToFalse(valid_nama);
		}else{
			setToTrue(valid_nama);
		}


		e.preventDefault();

		if (valid_nama == true && valid_urutan == true && valid_status == true) {
			$('#form_master').submit();
		}else{
   			errMsg('Data masih belum terisi semua!');
		}
	});
</script>
@stop
