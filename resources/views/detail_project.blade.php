@extends('layouts.app_frontend')

@section('content')



    <!-- navbar -->
    <nav class="navbar navbar-primary nav-alternate fixed-top">
        <a class="navbar-brand page-scroll" href="{{ url('/') }}"><img src="{{ asset('assets/frontend/images/PAPILIOtech..png') }}" alt=""></a>
        <ul class="nav btn-backtohome">
            <li class="nav-item text-center" id="btn-sidemenu">
                <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-arrow-left icon"></i> <span>Home</span> </a>
            </li>
        </ul>
        <ul class="nav ml-auto btn-socmed">
            <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->twitter }}"><i class="fab fa-twitter"></i></a></li>
            <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->fb }}"><i class="fab fa-facebook-f"></i></a></li>
            <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->ig }}"><i class="fab fa-instagram"></i></a></li>
            <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->linked_in }}"><i class="fab fa-linkedin"></i></a></li>
        </ul>
    </nav>

    <!-- head banner -->
    <section id="head-banner"></section>

    <!-- portfolio detail -->
    <section id="portfolio-detail">
        <div class="container">
            <div class="row justify-content-center portfolio-image">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <img class="img-fluid" src="{{ url('preview-file/foto-projects/'.$data->gambar) }}" alt="{{ $data->project }}" title="{{ $data->project }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center portfolio-content">
                <div class="col-12 col-lg-11 desc">
                    <h1 class="title">{{ $data->project }} </h1>
                    <a href="{{ $data->link }}" class="btn link-preview"><i class="fas fa-globe-asia"></i> Live Preview Site</a>

                    @php
                        echo $data->deskripsi;
                    @endphp

                    <div class="meta">
                        <div class="media">
                            <img src="{{ asset('assets/frontend/images/ic_category.png') }}" class="" alt="...">
                            <div class="media-body">
                                <h5 class="mt-0">Category :</h5>
                                <small>
                                    @php
                                    $n_catg = count($categories);
                                    $n_c = 1;
                                    @endphp
                                    @foreach ($categories as $item)
                                        {{$item->categories}}
                                        @if($n_c != $n_catg)
                                        ,
                                         @elseif($n_c == $n_catg)

                                         @endif
                                         @php
                                             $n_c++;
                                         @endphp
                                    @endforeach
                                </small>
                            </div>
                        </div>
                        <div class="media">
                            <img src="{{ asset('assets/frontend/images/ic_tags.png') }}" class="" alt="...">
                            <div class="media-body">
                                <h5 class="mt-0">Tags :</h5>
                                <small>
                                    @php
                                        $n_tags = count($tags);
                                        $n = 1;
                                    @endphp
                                    @foreach ($tags as $item)
                                        {{ $item->categories}}
                                        @if($n != $n_tags)
                                       ,
                                        @elseif($n == $n_tags)

                                        @endif
                                        @php
                                            $n++;
                                        @endphp
                                    @endforeach
                                </small>
                            </div>
                        </div>
                    </div>
                    <a href="{{ url('/project') }}" class="btn link-back"> <i class="fas fa-arrow-left"></i> Back to Portfolio & Projects</a>

                </div>
                <!-- <div class="col-12 col-lg-3 latest-projects">
                    <div class="card">
                        <div class="card-body">
                            <h4>Latest Projects</h4>

                            <div class="media">
                                <div class="media-img">
                                    <img src="images/portfolio/Mask Group 1.png" class="img-fluid mr-3" alt="...">
                                </div>
                                <div class="media-body">
                                    <h5 class="mt-0"><a href="#"> Project title </a></h5>
                                    <span>Web Development</span>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-img">
                                    <img src="images/portfolio/Mask Group 1.png" class="img-fluid mr-3" alt="...">
                                </div>
                                <div class="media-body">
                                    <h5 class="mt-0"><a href="#"> Project title </a></h5>
                                    <span>Web Development</span>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-img">
                                    <img src="images/portfolio/Mask Group 2.png" class="img-fluid mr-3" alt="...">
                                </div>
                                <div class="media-body">
                                    <h5 class="mt-0"><a href="#"> Project title </a></h5>
                                    <span>Web Design</span>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-img">
                                    <img src="images/portfolio/Mask Group 4.png" class="img-fluid mr-3" alt="...">
                                </div>
                                <div class="media-body">
                                    <h5 class="mt-0"><a href="#"> Project title </a></h5>
                                    <span>UI/UX</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>




@endsection

