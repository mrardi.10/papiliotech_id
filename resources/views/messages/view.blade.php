@extends('layouts.app_backend')

@section('title')
View Messages
@stop

@section('title_page')
View Messages
@stop

@section('css_after')
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/dropify/dropify.min.css')}}">
@stop

@section('content')
<section class="content">
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">View Messages from <span class="blue">{{ $data->full_name }}</span></h3><br>
              </div>
              <!-- /.box-header -->
              <!-- form start -->

                <div class="box-body">

                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="client" class="blue mr-3">Email</label>
                                    </div>
                                    <div class="col-md-8">
                                        <a href="mailto:{{ $data->email }}" target="_blank" class="black">
                                            {{ $data->email }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="client" class="blue mr-3">Phone Number</label>
                                    </div>
                                    <div class="col-md-8">
                                        <a href="https://api.whatsapp.com/send?phone=62{{ $data->phone_number }}" target="_blank" class="black">
                                            {{ $data->phone_number }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="client" class="blue mr-3">Message</label>
                                    </div>
                                    <div class="col-md-8">
                                        {{ $data->message }}
                                    </div>
                                </div>
                            </div>
                        </div>

						</div>
                  	</div>
                <!-- /.box-body -->
                @if($data->status == 1)
                <div class="box-footer">
                <a href="{{ url('change-status/'.$id.'/messages') }}">
                  <button type="button" class="btn bg-primary">Mark as Read</button>
                </a>
                </div>
                @endif

            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
</section>
@stop

@section('js_after')

<script src="{{ asset('assets/backend/plugins/dropify/dropify.min.js') }}"></script>
<script type="text/javascript">

	$(document).ready(function(){
        // Basic
        $('.dropify').dropify();
         // Translated
         $('.dropify-fr').dropify({
            messages: {
               default: 'Glissez-déposez un fichier ici ou cliquez',
               replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
               remove:  'Supprimer',
               error:   'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element){
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element){
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element){
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e){
            e.preventDefault();
            if (drDestroy.isDropified()) {
               drDestroy.destroy();
            } else {
               drDestroy.init();
            }
        })
	});

    var valid_client = new Boolean(false);
    var valid_link = new Boolean(false);
    var valid_urutan = new Boolean(false);
    var valid_gambar = new Boolean(true);
    var valid_status = new Boolean(false);

	$('#client').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_client);
			$('#errClient').html("Client tidak boleh Kosong");

		}else{
			$('#errClient').html("");
			setToTrue(valid_client);
		}
	});

    $('#link').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_link);
			$('#errLink').html("Link tidak boleh Kosong");

		}else{
			$('#errLink').html("");
			setToTrue(valid_link);
		}
	});

    $('#urutan').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_nama);
			$('#errUrutan').html("Urutan tidak boleh Kosong");

		}else{
			$('#errUrutan').html("");
			setToTrue(valid_nama);
		}
	});

    $('#status').on('change',function(){
		if ($(this).val() == "") {
			$('#errStatus').html("Status tidak boleh Kosong");
			setToFalse(valid_status);

		}else{
			$('#errStatus').html("");
			setToTrue(valid_status);
		}
	});

    $('.gambar').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }else {
            $('#errGambar').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_gambar);
        }
    });

	$('#btn_submit').on('click',function(e){


		if ($('#client').val().length == 0) {
			setToFalse(valid_client);
		}else{
			setToTrue(valid_client);
		}

        if ($('#link').val().length == 0) {
			setToFalse(valid_link);
		}else{
			setToTrue(valid_link);
		}

        if ($('#urutan').val().length == 0) {
			setToFalse(valid_urutan);
		}else{
			setToTrue(valid_urutan);
		}

        if ($('#status').val().length == 0) {
			setToFalse(valid_status);
		}else{
			setToTrue(valid_status);
		}

		e.preventDefault();

		if (valid_client == true && valid_link == true && valid_urutan == true && valid_status == true && valid_gambar == true) {
			$('#form_master').submit();
		}else{
   			errMsg('Data masih belum terisi semua!');
		}
	});
</script>
@stop
