@extends('layouts.app_frontend')

@section('content')

    <!-- navbar -->
    <nav class="navbar navbar-primary nav-alternate fixed-top">
        <a class="navbar-brand page-scroll" href="{{ url('/') }}"><img src="{{ asset('assets/frontend/images/PAPILIOtech..png') }}" alt=""></a>
        <ul class="nav btn-backtohome">
            <li class="nav-item text-center" id="btn-sidemenu">
                <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-arrow-left icon"></i> <span>Home</span> </a>
            </li>
        </ul>
        <ul class="nav ml-auto btn-socmed">
            <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->twitter }}"><i class="fab fa-twitter"></i></a></li>
            <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->fb }}"><i class="fab fa-facebook-f"></i></a></li>
            <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->ig }}"><i class="fab fa-instagram"></i></a></li>
            <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->linked_in }}"><i class="fab fa-linkedin"></i></a></li>
        </ul>
    </nav>

    <!-- portfolio list -->
    <section id="portfolio-list">
        <div class="container-fluid">
            <div class="row portfolio-nav">
                <div class="col-lg-12 filter-nav">
                    <div class="toggle-filter d-flex flex-row justify-content-between">
                        <h1>Our Portfolio & Projects</h1>
                        <a class="btn btn-filter" type="button" data-toggle="collapse"
                            data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fas fa-filter"></i>
                            Filter
                        </a>
                    </div>
                    <div class="collapse" id="collapseExample">
                        <div class="card card-body">
                            <ul class="nav justify-content-center nav-pills">
                                <li class="nav-item">
                                    <a id="btn-filter" data-filter="all" class="nav-link active" href="#">All</a>
                                </li>
                                @foreach ($catg as $item)
                                <li class="nav-item">
                                    <a id="btn-filter" data-filter="{{ str_replace(' ','-',$item->categories) }}" class="nav-link" href="#">{{ $item->categories }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center portfolio-item">
                @if (count($project) > 0)
                @foreach ($project as $item)
                @php
                    $gambar = $item->gambar;
                @endphp
                @php
                $n_catg = count($categories);
                $n_c = 1;
                @endphp

                <div class="col-12 col-md-6 col-lg-4 item @foreach($categories as $item)@if($item->id == $item->id_project){{ str_replace(' ','-',$item->categories) }}@endif @endforeach">
                    <div class="card">
                        <img src="{{ url('preview-file/foto-projects/'.$gambar) }}" class="card-img-top img-fluid" alt="" title="">
                        <div class="card-body">
                            <h4>
                               {{$item->project}}
                            </h4>
                            <small>
                                @php
                                $n_catg = count($categories);
                                $n_c = 1;
                                @endphp
                                @foreach ($categories as $item)
                                @if($item->id == $item->id_project)
                                    {{$item->categories}}
                                    @if($n_c != $n_catg)
                                    ,
                                     @elseif($n_c == $n_catg)

                                     @endif
                                     @php
                                         $n_c++;
                                     @endphp
                                @endif
                                @endforeach
                            </small>
                            <div>
                                <a href="{{ url('preview-file/foto-projects/'.$gambar) }}" class="btn btn-sm btn-zoom venobox preview-link" data-gall="GalleryPortfolio" title=" Title image here"><i class="fas fa-search-plus"></i> Zoom</a>
                                <a href="{{ url('detail-project/'.$item->id) }}" class="btn btn-sm btn-detail"><i class="fas fa-info-circle"></i>  Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="col-12 col-md-6 col-lg-4 item web-development">
                    <div class="card">
                        <img src="{{ asset('assets/frontend/images/portfolio/Mask Group 1.png') }}" class="card-img-top img-fluid" alt="" title="">
                        <div class="card-body">
                            <h4>
                                Portfolio 1
                            </h4>
                            <small>Web Development</small>
                            <div>
                                <a href="{{ asset('assets/frontend/images/portfolio/Mask Group 1.png') }}" class="btn btn-sm btn-zoom venobox preview-link" data-gall="GalleryPortfolio" title=" Title image here"><i class="fas fa-search-plus"></i> Zoom</a>
                                <a href="portfolio-detail.html" class="btn btn-sm btn-detail"><i class="fas fa-info-circle"></i>  Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif




                <div class="col-12 text-center py-4">
                    <button class="btn btn-loadmore">Load More</button>
                </div>
            </div>
        </div>
    </section>

    @endsection
