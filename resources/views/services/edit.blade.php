@extends('layouts.app_backend')

@section('title')
Ubah Service
@stop

@section('title_page')
Ubah Service
@stop

@section('css_after')
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/dropify/dropify.min.css')}}">
@stop

@section('content')
<section class="content">
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Data Service</h3><br>
                <p>Pastikan yang merupakan tanda (*) diisi dengan benar</p>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="" method="post" id="form_master" enctype="multipart/form-data">
              	@csrf
                <div class="box-body">

                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                  			    <label for="service">Service <span class="wajib">*</span></label>
							    <input type="text" name="service" class="form-control" id="service" value="{{ $data->service }}">
                  			    <span class="err" id="errService"></span>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi <span class="wajib">*</span></label>
                                <input type="text" name="deskripsi" class="form-control" id="deskripsi" value="{{ $data->deskripsi }}">
                                <span class="err" id="errDeskripsi"></span>
                            </div>
                            <div class="form-group">
                                <label for="">Gambar <span class="wajib">*</span></label>
                                @if($data->gambar != "")
                                <input type="file" id="input-file-now-custom-1" class="dropify gambar" data-default-file="{{url('/')}}/storage/app/public/foto-services/{{ $data->gambar }}" name="gambar"/>
                                @else
                                <input type="file" id="input-file-now-custom-1" class="dropify gambar" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="gambar"/>
                                @endif
                                <input type="hidden" value="{{ $data->gambar }}" name="old_gambar">
								<span class="err" id="errGambar"></span>
                            </div>
                            <div class="form-group">
								<label for="status">Status <span class="wajib">*</span></label>
								<select class="form-control" id="status" name="status">
									<option value="" {{ $data->status == '' ? 'selected' : '' }}>-----</option>
									<option value="1" {{ $data->status == 1 ? 'selected' : '' }}>Aktif</option>
									<option value="0" {{ $data->status == 0 ? 'selected' : '' }}>Tidak Aktif</option>
								</select>
								<span class="err" id="errStatus"></span>
							  </div>
						</div>
                  	</div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="button" class="btn bg-primary" id="btn_submit">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
</section>
@stop

@section('js_after')

<script src="{{ asset('assets/backend/plugins/dropify/dropify.min.js') }}"></script>
<script type="text/javascript">

	$(document).ready(function(){
        // Basic
        $('.dropify').dropify();
         // Translated
         $('.dropify-fr').dropify({
            messages: {
               default: 'Glissez-déposez un fichier ici ou cliquez',
               replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
               remove:  'Supprimer',
               error:   'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element){
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element){
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element){
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e){
            e.preventDefault();
            if (drDestroy.isDropified()) {
               drDestroy.destroy();
            } else {
               drDestroy.init();
            }
        })
	});

    var valid_service = new Boolean(false);
    var valid_deskripsi = new Boolean(false);
    @if($data->gambar != "")
    var valid_gambar = new Boolean(true);
    @else
    var valid_gambar = new Boolean(false);
    @endif
    var valid_status = new Boolean(false);

	$('#service').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_service);
			$('#errService').html("Service tidak boleh Kosong");

		}else{
			$('#errService').html("");
			setToTrue(valid_service);
		}
	});

    $('#deskripsi').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_deskripsi);
			$('#errDeskripsi').html("Deskripsi tidak boleh Kosong");

		}else{
			$('#errDeskripsi').html("");
			setToTrue(valid_deskripsi);
		}
	});

    $('#status').on('change',function(){
		if ($(this).val() == "") {
			$('#errStatus').html("Status tidak boleh Kosong");
			setToFalse(valid_status);

		}else{
			$('#errStatus').html("");
			setToTrue(valid_status);
		}
	});

    $('.gambar').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }else {
            $('#errGambar').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_gambar);
        }
    });

	$('#btn_submit').on('click',function(e){


		if ($('#service').val().length == 0) {
			setToFalse(valid_service);
		}else{
			setToTrue(valid_service);
		}

        if ($('#deskripsi').val().length == 0) {
			setToFalse(valid_deskripsi);
		}else{
			setToTrue(valid_deskripsi);
		}

        if ($('#status').val().length == 0) {
			setToFalse(valid_status);
		}else{
			setToTrue(valid_status);
		}

		e.preventDefault();

		if (valid_service == true && valid_deskripsi == true && valid_status == true && valid_gambar == true) {
			$('#form_master').submit();
		}else{
   			errMsg('Data masih belum terisi semua!');
		}
	});
</script>
@stop
