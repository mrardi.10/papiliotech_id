@extends('layouts.app_frontend')

@section('content')

 <!-- navbar -->
 <nav class="navbar navbar-primary fixed-top">
    <a class="navbar-brand page-scroll" href="#home"><img src="{{ asset('assets/frontend/images/PAPILIOtech..png') }}" alt=""></a>
    <ul class="nav">
        <li class="nav-item btn-sidemenu" id="btn-sidemenu">
            <button class="btn nav-link" href="#"><img src="{{ asset('assets/frontend/images/icon_menu.png') }}" alt=""></button>
        </li>
    </ul>
    <ul class="nav ml-auto d-none d-sm-block">
        <li class="nav-item btn-portfolio">
            <a class="nav-link page-scroll" href="#portfolio">PORTFOLIO</a>
        </li>
    </ul>
</nav>

<!-- sidenav menu -->
<aside id="sidebar">
    <header>
        <img src="{{ url('preview-file/foto-web/'.$apps->logo_sidebar) }}" alt="">
        {{-- <img src="{{ storage_path('foto-web/'.$apps->logo_sidebar) }}" alt=""> --}}
        <!-- <hr> -->
        <button href="#" class="btn btn-hidesidebar" id="btn-closesidebar"><i class="fas fa-times"></i></button>
    </header>

    <ul class="menu">
        {{-- @if(count($menus) > 0)
        @foreach($menus as $menu)
        <li><a class="page-scroll" class="active" href="#{{ $menu->link }}">{{ $menu->nama }}</a></li>
        @endforeach
        @else
        <li><a class="page-scroll" href="#">Tidak ada Menu</a></li>
        @endif --}}
        <li><a class="page-scroll" href="#hero">Home</a></li>
        <li><a class="page-scroll" href="#about">About Us</a></li>
        <li><a class="page-scroll" href="#services">Services</a></li>
        <li><a class="page-scroll" href="#portfolio">Portfolio</a></li>
        <li><a class="page-scroll" href="#testimonials">Testimonials</a></li>
        <li><a class="page-scroll" href="#blog">Blog</a></li>
        <li><a class="page-scroll" href="#contact">Contact Us</a></li>
    </ul>

    <div class="footer-sidemenu">
        <span>&copy; {{ date('Y').' '.$apps->name }}</span>
        <div class="socmed">
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-facebook-f"></i></a>
            <a href="#"><i class="fab fa-instagram"></i></a>
            <a href="#"><i class="fab fa-linkedin"></i></a>
        </div>
    </div>
</aside>
<!-- hero -->
<section id="hero">
    <div class="container-fluid">
        <div class="row">
            <div class=" col-lg-1 socmed d-none d-sm-block">
                <ul>
                    <li><a href="#"><img src="{{ asset('assets/frontend/images/ic_facebook.png') }}"></a></li>
                    <li><a href="#"><img src="{{ asset('assets/frontend/images/ic_twitter.png') }}"></a></li>
                    <li><a href="#"><img src="{{ asset('assets/frontend/images/ic_instagram.png') }}"></a></li>
                    <li><a href="#"><img src="{{ asset('assets/frontend/images/ic_linkedin.png') }}"></a></li>
                </ul>
            </div>
            <div class="col-lg-6 caption-hero" data-aos="fade-up">
                <h1>
                    <span>Build</span> Your <span>Plan</span> With <br>
                    Our <span>Process</span> like <br>
                    a <span>Butterfly</span>
                </h1>
                <a href="#about" class="btn btn-hero page-scroll"> Tell Me More</a>
            </div>
            <div class="col-lg-6 logo-hero">
                <img src="{{ url('preview-file/foto-web/'.$apps->logo) }}" alt="papiliotech" data-aos="fade-up" data-aos-delay="500">
            </div>
        </div>
    </div>
</section>

<!-- about us -->
<section id="about">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 about-image" style="background-image: url({{ url('preview-file/foto-web/'.$apps->gambar_tentang) }});"></div>
            <div class="col-lg-7 about-caption">
                <ul class="nav nav-pills nav-justified mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="btn-about flex-sm-fill active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                            role="tab" aria-controls="pills-home" aria-selected="true"><img src="{{ asset('assets/frontend/images/ic_whyus.png') }}" class="mr-1"> Why Us ?</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn-about flex-sm-fill" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                            role="tab" aria-controls="pills-profile" aria-selected="false"><img src="{{ asset('assets/frontend/images/ic_history.png') }}" class="mr-1"> Our History</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                        aria-labelledby="pills-home-tab">
                        <h3>{{ $apps->name }}</h3>
                        <p>
                            @php
                                echo $apps->tentang_kita;
                            @endphp
                        </p>

                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                        aria-labelledby="pills-profile-tab">
                        <h3>{{ $apps->name }}</h3>
                        <p>
                            @php
                                echo $apps->sejarah;
                            @endphp
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Clients -->
<section id="clients">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 caption-clients">
                <h2>Our Clients</h2>
                <a href="#" class="">View More</a>
            </div>
            <div class="col-sm-12 col-lg-10 clients-item">
                <!-- slider owl carousel -->
                <div class="owl-carousel owl-theme box-clients">
                    @if (count($clients) > 0)
                    @foreach ($clients as $item)
                        <div class="item img-item">
                            <img class="client" src="{{ url('preview-file/logo-clients/'.$item->logo) }}" alt="">
                        </div>
                    @endforeach
                    @else
                    <div class="item img-item">
                        <img class="client" src="images/clients/microsoft_logo.png" alt="">
                    </div>
                    @endif
                    {{-- <div class="item img-item">
                        <img class="client" src="images/clients/microsoft_logo.png" alt="">
                    </div>
                    <div class="item img-item">
                        <img class="client" src="images/clients/lenovo_png.png" alt="">
                    </div>
                    <div class="item img-item">
                        <img class="client" src="images/clients/hp_logo.png" alt="">
                    </div>
                    <div class="item img-item">
                        <img class="client" src="images/clients/tencentgame_logo.png" alt="">
                    </div>
                    <div class="item img-item">
                        <img class="client" src="images/clients/Dell_Logo.png" alt="">
                    </div>
                    <div class="item img-item">
                        <img class="client" src="images/clients/samsung_logo.png" alt="">
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>

<!-- services -->
<section id="services">
    <div class="container-fluid">
        <div class="row head-services">
            <div class="col-12">
                <header class="text-center">
                    <h2>OUR SERVICES</h2>
                </header>
            </div>
        </div>

        <div class="row justify-content-center item-services">
            @if (count($services) > 0)
            @foreach ($services as $item)
            <div class="col-md-6 col-lg-3">
                <div class="card text-center" data-aos="fade-up">
                    <div class="card-header">
                        <img src="{{ url('preview-file/foto-services/'.$item->gambar) }}" alt="">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{ $item->service }}</h5>
                        <p class="card-text">{{ $item->deskripsi}}</p>
                    </div>
                    <!-- <div class="card-footer p-0">
                        <a href="#" class="btn btn-services btn-block">Read More</a>
                    </div> -->
                </div>
            </div>
            @endforeach

            @else
            <div class="col-md-6 col-lg-3">
                <div class="card text-center" data-aos="fade-up">
                    <div class="card-header">
                        <img src="{{ asset('assets/frontend/images/web_design.png') }}" alt="">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Web Design</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional
                            content.</p>
                    </div>
                    <!-- <div class="card-footer p-0">
                        <a href="#" class="btn btn-services btn-block">Read More</a>
                    </div> -->
                </div>
            </div>
            @endif


        </div>
    </div>
</section>

<!-- portfolio -->
<section id="portfolio">
    <div class="container-fluid">
        <div class="row head-portfolio">
            <div class="col-12">
                <header class="text-center">
                    <h2>PROJECTS & PORTFOLIO</h2>
                </header>
            </div>
        </div>

        <div class="row justify-content-center item-portfolio" data-aos="fade-up">
            @if (count($project)>0)
                @foreach ($project as $item)
                <div class="col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <img src="{{ url('preview-file/foto-projects/'.$item->gambar) }}" class="card-img-top img-fluid" alt="">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $item->project }}</h5>
                            <a href="{{ url('detail-project/'.$item->id) }}">View Detail</a>
                        </div>
                        <div class="card-footer">
                            <img src="{{ asset('assets/frontend/images/papilio_card.png') }}" alt="">
                        </div>
                    </div>
                </div>
                @endforeach

            @else
            <div class="col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <img src="images/portfolio/Mask Group 6.png" class="card-img-top img-fluid" alt="">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <a href="portfolio-detail.html">View Detail</a>
                    </div>
                    <div class="card-footer">
                        <img src="images/papilio_card.png" alt="">
                    </div>
                </div>
            </div>
            @endif



        </div>
        @if(count($project) > 3)
        <div class="row text-center">
            <div class="col-12">
                <a href="{{ url('project') }}" class="btn btn-viewmore">View More Porfolio & Projects</a>
            </div>
        </div>
        @endif
    </div>
</section>

<!-- testimonials -->
<section id="testimonials">
    <div class="container-fluid">
        <div class="row head-testimonial">
            <div class="col-12">
                <header class="text-center">
                    <h2>TESTIMONIALS</h2>
                </header>
            </div>
        </div>
        <div class="row testimonial-item" data-aos="fade-up">
            <div class="col-lg-12">
                <div class="owl-carousel owl-theme box-testimonials">
                    @if (count($testimonials) > 0)
                    @foreach ($testimonials as $item)
                    <div class="item">
                        <div class="card text-center">
                            <div class="card-header">
                                <div class="img-rounded">
                                    <img src="{{ url('preview-file/foto-testimonials/'.$item->foto) }}">
                                </div>
                            </div>
                            <div class="card-body">
                                <h5>{{ $item->nama }}</h5>
                                <small>{{ $item->dari }}</small>
                                <p>
                                    {{ $item->testi }}
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="item">
                        <div class="card text-center">
                            <div class="card-header">
                                <div class="img-rounded">
                                    <img src="images/t2.jpg">
                                </div>
                            </div>
                            <div class="card-body">
                                <h5>Rokie Charles</h5>
                                <small>Venezuela Group</small>
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit inventore omnis, deserunt quia facere quidem eos optio reprehenderit ratione fugiat alias quis quos dignissimos illo, quas incidunt. Eos, vitae aliquam.
                                </p>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            @if(count($testimonials) > 3)
            <div class="col-lg-12 mt-5 mb-3 text-center">
                <a href="{{ url('testimonials') }}" class="btn btn-viewmore">View More Testimonials</a>
            </div>
            @endif
        </div>
    </div>
</section>

<!-- contact us -->
<section id="contact">
    <div class="container">
        <div class="row head-contact text-center">
            <div class="col-12">
                <header>
                    <h2>Let's <span>Talk</span> With Us</h2>
                </header>
            </div>
        </div>
        <div class="row justify-content-center contact-item">
            <div class="col-md-6 col-lg-5">
                <div class="card">
                    <div class="card-body">
                        <div class="media">
                            <img src="{{ asset('assets/frontend/images/ic_contact.png') }}" class="mr-3" alt="...">
                            <div class="media-body">
                                <h5 class="mt-0">Contact</h5>
                                <span>+62 {{ $apps->no_hp }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-5">
                <div class="card">
                    <div class="card-body">
                        <div class="media">
                            <img src="{{ asset('assets/frontend/images/ic_email.png') }}" class="mr-3" alt="...">
                            <div class="media-body">
                                <h5 class="mt-0">Email</h5>
                                <span>{{ $apps->email }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center contact-form" id="messages">
            <div class="col-lg-10">
                <div class="card">
                    <h5>Or Send <span> Message </span> to Us</h5>
                    <div class="card-body">
                        <form action="{{ url('send-messages') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-sm-3 col-md col-lg form-group">
                                    <input type="text" name="full_name" class="form-control" placeholder="Full Name" required>
                                </div>
                                <div class="col-sm-3 col-md col-lg form-group">
                                    <input type="text" name="phone_number" class="form-control" placeholder="Phone Number" required onkeypress="numberOnly(event)">
                                </div>
                                <div class="col-sm-3 col-md col-lg form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="10" placeholder="Type Message"></textarea>
                            </div>
                            <div class="form-group">
                                {!! NoCaptcha::renderJs() !!}
                                {!! NoCaptcha::display() !!}
                            </div>

                            <button type="submit" class="btn btn-message btn-block">Send Message</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- blog -->
<section id="blog">
    <div class="container-fluid">
        <div class="row head-blog text-center">
            <div class="col-12">
                <header>
                    <h2>BLOG</h2>
                </header>
            </div>
        </div>
        <div class="row justify-content-center blog-item" data-aos="fade">
            @if(count($blog) > 0)
            @foreach($blog as $item)
            <div class="col-12 col-md-6 col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <img src="{{ url('preview-file/foto-blog/'.$item->gambar) }}" class="" alt="" title>
                    </div>
                    <div class="card-body">
                        <h4> <a href="{{ url('view-blog/'.$item->id) }}"> {{ $item->judul }} </a></h4>
                        <span>
                            <i class="fas fa-clock"></i>
                            {{ Helper::bulan_tanggal_en($item->tanggal) }}, {{ date('Y',strtotime($item->tanggal)) }}
                        </span>
                    </div>
                </div>
            </div>
            @endforeach
            @else
             <div class="col-12 col-md-6 col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <img src="{{ asset('assets/frontend/images/blog/campaign-creators-iEiUITs149M-unsplash.jpg') }}" class="" alt="" title>
                    </div>
                    <div class="card-body">
                        <h4> <a href="detail-blog.html"> Lorem ipsum dolor sit. </a></h4>
                        <span>
                            <i class="fas fa-clock"></i>
                            May 21, 2020
                        </span>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-12 mt-5 text-center">
                <a href="{{ url('blog') }}" class="btn btn-viewmore">View More Post</a>
            </div>
        </div>
    </div>
</section>

<!-- team -->
<section id="team">
    <div class="container-fluid">
        <div class="row">
            @if (count($founder) > 0)
            @foreach ($founder as $item)
            <div class="col-lg-6 team-item">
                <div class="card">
                    <div class="card-header">
                        <div class="img-rounded">
                            <img src="{{ url('preview-file/foto-founder/'.$item->gambar) }}" alt="{{ $item->nama }}">
                        </div>
                    </div>
                    <div class="card-body">
                        <h4>{{ $item->nama }}</h4>
                        <span>{{ $item->jabatan }}</span>
                        <br>
                        <span>{{ $apps->name }}</span>
                    </div>
                </div>
            </div>
            @endforeach
            @else

            <div class="col-lg-6 team-item">
                <div class="card">
                    <div class="card-header">
                        <div class="img-rounded">
                            <img src="{{ asset('assets/frontend/images/tedy.jpg') }}" alt="papiliotech">
                        </div>
                    </div>
                    <div class="card-body">
                        <h4>TEDY HIDAYAT</h4>
                        <span>CEO FOUNDER & FRONTEND</span>
                        <br>
                        <span>Papiliotech</span>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>

@endsection
