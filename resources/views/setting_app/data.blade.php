@extends('layouts.app_backend')

@section('title')
Setting Web
@stop

@section('title_page')
Setting Web
@stop

@section('css_after')
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/dropify/dropify.min.css')}}">
@stop

@section('content')
<section class="content">
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Data Website</h3><br>
                <p>Pastikan yang merupakan tanda (*) diisi dengan benar</p>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="" method="post" id="form_master" enctype="multipart/form-data">
              	@csrf
                <div class="box-body">

                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                  			    <label for="name">Nama <span class="wajib">*</span></label>
                                @if ($data != "")
                                    <input type="text" name="name" class="form-control" id="name" value="{{ $data->name }}">
                                @else
                                    <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}">
                                @endif
                  			    <span class="err" id="errName"></span>
                            </div>
                            <div class="form-group">
                                <label for="version">Version <span class="wajib">*</span></label>
                                @if ($data != "")
                                    <input type="text" name="version" class="form-control" id="version" value="{{ $data->version }}">
                                @else
                                    <input type="text" name="version" class="form-control" id="version" value="{{ old('version') }}">
                                @endif
                                <span class="err" id="errVersion"></span>
                            </div>
                            <div class="form-group">
                                <label for="tentang_kita">Tentang Brand <span class="wajib">*</span></label>
                                @if ($data != "")
                                    <textarea name="tentang_kita" class="form-control" id="tentang_kita" cols="30" rows="5">{{ $data->tentang_kita }}</textarea>
                                @else
                                    <textarea name="tentang_kita" class="form-control" id="tentang_kita" cols="30" rows="5">{{ old('tentang_kita') }}</textarea>
                                @endif
                                <span class="err" id="errTentangKita"></span>
                            </div>
                            <div class="form-group">
                                <label for="sejarah">Sejarah <span class="wajib">*</span></label>
                                @if ($data != "")
                                    <textarea name="sejarah" class="form-control" id="sejarah" cols="30" rows="5">{{ $data->sejarah }}</textarea>
                                @else
                                    <textarea name="sejarah" class="form-control" id="sejarah" cols="30" rows="5">{{ old('sejarah') }}</textarea>
                                @endif
                                <span class="err" id="errSejarah"></span>
                            </div>
                            <div class="form-group">
                                <label for="email">Email <span class="wajib">*</span></label>
                                @if ($data != "")
                                    <input type="email" name="email" class="form-control" id="email" value="{{ $data->email }}">
                                @else
                                    <input type="email" name="email" class="form-control" id="email" value="{{ old('email') }}">
                                @endif
                                <span class="err" id="errEmail"></span>
                            </div>
                            <div class="form-group">
                                <label for="meta_keyword">Meta Keyword <span class="wajib">*</span></label>
                                @if ($data != "")
                                    <input type="text" name="meta_keyword" class="form-control" id="meta_keyword" value="{{ $data->meta_keyword }}">
                                @else
                                    <input type="text" name="meta_keyword" class="form-control" id="meta_keyword" value="{{ old('meta_keyword') }}">
                                @endif
                                <span class="err" id="errMetaKeyword"></span>
                            </div>
                            <div class="form-group">
                                <label for="">Logo <span class="wajib">*</span></label>
                                @if ($data != "")
                                    @if ($data->logo != "")
                                    <input type="file" id="input-file-now-custom-1" class="dropify logo" data-default-file="{{url('/')}}/storage/app/public/foto-web/{{ $data->logo }}" name="logo"/>
                                    <input type="hidden" class="" value="{{ $data->logo }}" name="old_logo"/>
                                    @else
                                    <input type="file" id="input-file-now-custom-1" class="dropify logo" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="logo"/>
                                    @endif
                                @else
                                <input type="file" id="input-file-now-custom-1" class="dropify logo" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="logo"/>
                                @endif
								<span class="err" id="errLogo"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                			<div class="form-group">
                  			    <label for="fb">Facebook</label>
                                @if ($data != "")
                                    <input type="url" name="fb" class="form-control" id="fb" value="{{ $data->fb }}">
                                @else
                                    <input type="url" name="fb" class="form-control" id="fb" value="{{ old('fb') }}">
                                @endif
                  			    <span class="err" id="errFb"></span>
                            </div>
                            <div class="form-group">
                                <label for="twitter">Twitter</label>
                                @if ($data != "")
                                    <input type="url" name="twitter" class="form-control" id="twitter" value="{{ $data->twitter }}">
                                @else
                                    <input type="url" name="twitter" class="form-control" id="twitter" value="{{ old('twitter') }}">
                                @endif
                                <span class="err" id="errTwitter"></span>
                            </div>
                            <div class="form-group">
                                <label for="ig">Instagram </label>
                                @if ($data != "")
                                    <input type="url" name="ig" class="form-control" id="ig" value="{{ $data->ig }}">
                                @else
                                    <input type="url" name="ig" class="form-control" id="ig" value="{{ old('ig') }}">
                                @endif
                                <span class="err" id="errIg"></span>
                            </div>
                            <div class="form-group">
                                <label for="linked_in">Linked IN</label>
                                @if ($data != "")
                                    <input type="url" name="linked_in" class="form-control" id="linked_in" value="{{ $data->linked_in }}">
                                @else
                                    <input type="url" name="linked_in" class="form-control" id="linked_in" value="{{ old('linked_in') }}">
                                @endif
                                <span class="err" id="errLinkedin"></span>
                            </div>
                            <div class="form-group">
                                <label for="jabatan">Alamat <span class="wajib">*</span></label>
                                @if ($data != "")
                                    <textarea name="alamat" class="form-control" id="alamat" cols="30" rows="5">{{ $data->alamat }}</textarea>
                                @else
                                    <textarea name="alamat" class="form-control" id="alamat" cols="30" rows="5">{{ old('alamat') }}</textarea>
                                @endif
                                <span class="err" id="errAlamat"></span>
                            </div>
                            <div class="form-group">
                                <label for="jabatan">No HP <span class="wajib">*</span></label>
                                @if ($data != "")
                                    <input type="tel" name="no_hp" class="form-control" onchange="checkNoHp(this)" onkeypress="numberOnly(event)" id="no_hp" value="{{ $data->no_hp }}">
                                @else
                                    <input type="tel" name="no_hp" class="form-control" onchange="checkNoHp(this)" onkeypress="numberOnly(event)" id="no_hp" value="{{ old('no_hp') }}">
                                @endif
                                <span class="err" id="errNoHp"></span>
                            </div>
                            <div class="form-group">
                                <label for="meta_description">Meta Description <span class="wajib">*</span></label>
                                @if ($data != "")
                                    <input type="text" name="meta_description" class="form-control" id="meta_description" value="{{ $data->meta_description }}">
                                @else
                                    <input type="text" name="meta_description" class="form-control" id="meta_description" value="{{ old('meta_description') }}">
                                @endif
                                <span class="err" id="errMetaDescription"></span>
                            </div>
                            <div class="form-group">
                                <label for="">Gambar Tentang <span class="wajib">*</span></label>
                                @if ($data != "")
                                    @if ($data->gambar_tentang != "")
                                    <input type="file" id="input-file-now-custom-1" class="dropify gambar_tentang" data-default-file="{{url('/')}}/storage/app/public/foto-web/{{ $data->gambar_tentang }}" name="gambar_tentang"/>
                                    <input type="hidden" class="" value="{{ $data->gambar_tentang }}" name="old_gambar_tentang"/>
                                    @else
                                    <input type="file" id="input-file-now-custom-1" class="dropify gambar_tentang" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="gambar_tentang"/>
                                    @endif
                                @else
                                    <input type="file" id="input-file-now-custom-1" class="dropify gambar_tentang" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="gambar_tentang"/>
                                @endif

								<span class="err" id="errGambarTentang"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Logo Header <span class="wajib">*</span></label>
                                @if ($data != "")
                                    @if ($data->logo_header != "")
                                    <input type="file" id="input-file-now-custom-1" class="dropify logo_header" data-default-file="{{url('/')}}/storage/app/public/foto-web/{{ $data->logo_header }}" name="logo_header"/>
                                    <input type="hidden" class="" value="{{ $data->logo_header }}" name="old_logo_header"/>
                                    @else
                                    <input type="file" id="input-file-now-custom-1" class="dropify logo_header" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="logo_header"/>
                                    @endif
                                @else
                                <input type="file" id="input-file-now-custom-1" class="dropify logo_header" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="logo_header"/>
                                @endif
								<span class="err" id="errLogoHeader"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Logo Footer <span class="wajib">*</span></label>
                                @if ($data != "")
                                    @if ($data->logo_footer != "")
                                    <input type="file" id="input-file-now-custom-1" class="dropify logo_footer" data-default-file="{{url('/')}}/storage/app/public/foto-web/{{ $data->logo_footer }}" name="logo_footer"/>
                                    <input type="hidden" class="" value="{{ $data->logo_footer }}" name="old_logo_footer"/>
                                    @else
                                    <input type="file" id="input-file-now-custom-1" class="dropify logo_footer" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="logo_footer"/>
                                    @endif
                                @else
                                <input type="file" id="input-file-now-custom-1" class="dropify logo_footer" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="logo_footer"/>
                                @endif
								<span class="err" id="errLogoFooter"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Logo Sidebar <span class="wajib">*</span></label>
                                @if ($data != "")
                                    @if ($data->logo_sidebar != "")
                                    <input type="file" id="input-file-now-custom-1" class="dropify logo_sidebar" data-default-file="{{url('/')}}/storage/app/public/foto-web/{{ $data->logo_sidebar }}" name="logo_sidebar"/>
                                    <input type="hidden" class="" value="{{ $data->logo_sidebar }}" name="old_logo_sidebar"/>
                                    @else
                                    <input type="file" id="input-file-now-custom-1" class="dropify logo_sidebar" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="logo_sidebar"/>
                                    @endif
                                @else
                                <input type="file" id="input-file-now-custom-1" class="dropify logo_sidebar" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="logo_sidebar"/>
                                @endif
								<span class="err" id="errLogoSidebar"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Icon <span class="wajib">*</span></label>
                                @if ($data != "")
                                    @if ($data->icon != "")
                                    <input type="file" id="input-file-now-custom-1" class="dropify icon" data-default-file="{{url('/')}}/storage/app/public/foto-web/{{ $data->icon }}" name="icon"/>
                                    <input type="hidden" class="" value="{{ $data->icon }}" name="old_icon"/>
                                    @else
                                    <input type="file" id="input-file-now-custom-1" class="dropify icon" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="icon"/>
                                    @endif
                                @else
                                <input type="file" id="input-file-now-custom-1" class="dropify icon" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="icon"/>
                                @endif
								<span class="err" id="errIcon"></span>
                            </div>
                        </div>
                  	</div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="button" class="btn bg-primary" id="btn_submit">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
</section>
@stop

@section('js_after')

<script src="{{ asset('assets/backend/plugins/dropify/dropify.min.js') }}"></script>
<script type="text/javascript">

	$(document).ready(function(){
        // Basic
        $('.dropify').dropify();
         // Translated
         $('.dropify-fr').dropify({
            messages: {
               default: 'Glissez-déposez un fichier ici ou cliquez',
               replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
               remove:  'Supprimer',
               error:   'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element){
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element){
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element){
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e){
            e.preventDefault();
            if (drDestroy.isDropified()) {
               drDestroy.destroy();
            } else {
               drDestroy.init();
            }
        })
    });

    var valid_name = new Boolean(false);
    var valid_version = new Boolean(false);
    var valid_tentang_kita = new Boolean(false);
    var valid_sejarah = new Boolean(false);
    @if($data->gambar_tentang != "")
    var valid_gambar_tentang = new Boolean(true);
    @else
    var valid_gambar_tentang = new Boolean(false);
    @endif
    @if($data->logo != "")
    var valid_logo = new Boolean(true);
    @else
    var valid_logo = new Boolean(false);
    @endif
    @if($data->logo_header != "")
    var valid_logo_header = new Boolean(true);
    @else
    var valid_logo_header = new Boolean(false);
    @endif
    @if($data->logo_sidebar != "")
    var valid_logo_sidebar = new Boolean(true);
    @else
    var valid_logo_sidebar = new Boolean(false);
    @endif
    @if($data->logo_footer != "")
    var valid_logo_footer = new Boolean(true);
    @else
    var valid_logo_footer = new Boolean(false);
    @endif
    @if($data->icon != "")
    var valid_icon = new Boolean(true);
    @else
    var valid_icon = new Boolean(false);
    @endif
    var valid_email = new Boolean(false);
    var valid_alamat = new Boolean(false);
    var valid_meta_keyword = new Boolean(false);
    var valid_meta_description = new Boolean(false);



	$('#name').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_name);
			$('#errName').html("Name tidak boleh Kosong");

		}else{
			$('#errName').html("");
			setToTrue(valid_name);
		}
	});

    $('#version').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_version);
			$('#errVersion').html("Version tidak boleh Kosong");

		}else{
			$('#errVersion').html("");
			setToTrue(valid_version);
		}
	});

    $('#tentang_kita').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_tentang_kita);
			$('#errTentangKita').html("Tentang Brand tidak boleh Kosong");

		}else{
			$('#errTentangKita').html("");
			setToTrue(valid_tentang_kita);
		}
	});

    $('#sejarah').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_sejarah);
			$('#errSejarah').html("Sejarah tidak boleh Kosong");

		}else{
			$('#errSejarah').html("");
			setToTrue(valid_sejarah);
		}
	});

    $('#email').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_email);
			$('#errEmail').html("Email tidak boleh Kosong");

		}else{
			$('#errEmail').html("");
			setToTrue(valid_email);
		}
	});

    $('#alamat').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_alamat);
			$('#errAlamat').html("Alamat tidak boleh Kosong");

		}else{
			$('#errAlamat').html("");
			setToTrue(valid_alamat);
		}
	});

    $('#meta_keyword').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_meta_keyword);
			$('#errMetaKeyword').html("Meta Keyword tidak boleh Kosong");

		}else{
			$('#errMetaKeyword').html("");
			setToTrue(valid_meta_keyword);
		}
	});

    $('#meta_description').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_meta_description);
			$('#errMetaDescription').html("Meta Description tidak boleh Kosong");

		}else{
			$('#errMetaDescription').html("");
			setToTrue(valid_meta_description);
		}
	});

    $('.gambar_tentang').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errGambarTentang').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar_tentang);
            }

            if (size_foto < 2096103) {
                $('#errGambarTentang').html("");
                setToTrue(valid_gambar_tentang);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errGambarTentang').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar_tentang);
            }

            if (size_foto < 2096103) {
                $('#errGambarTentang').html("");
                setToTrue(valid_gambar_tentang);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errGambarTentang').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar_tentang);
            }

            if (size_foto < 2096103) {
                $('#errGambarTentang').html("");
                setToTrue(valid_gambar_tentang);
            }
        }else {
            $('#errGambarTentang').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_gambar_tentang);
        }
    });

    $('.logo').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errLogo').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo);
            }

            if (size_foto < 2096103) {
                $('#errLogo').html("");
                setToTrue(valid_logo);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errLogo').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo);
            }

            if (size_foto < 2096103) {
                $('#errLogo').html("");
                setToTrue(valid_logo);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errLogo').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo);
            }

            if (size_foto < 2096103) {
                $('#errLogo').html("");
                setToTrue(valid_logo);
            }
        }else {
            $('#errLogo').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_logo);
        }
    });

    $('.logo_header').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errLogoHeader').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo_header);
            }

            if (size_foto < 2096103) {
                $('#errLogoHeader').html("");
                setToTrue(valid_logo_header);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errLogoHeader').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo_header);
            }

            if (size_foto < 2096103) {
                $('#errLogoHeader').html("");
                setToTrue(valid_logo_header);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errLogoHeader').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo_header);
            }

            if (size_foto < 2096103) {
                $('#errLogoHeader').html("");
                setToTrue(valid_logo_header);
            }
        }else {
            $('#errLogoHeader').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_logo_header);
        }
    });

    $('.logo_footer').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errLogoFooter').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo_footer);
            }

            if (size_foto < 2096103) {
                $('#errLogoFooter').html("");
                setToTrue(valid_logo_footer);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errLogoFooter').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo_footer);
            }

            if (size_foto < 2096103) {
                $('#errLogoFooter').html("");
                setToTrue(valid_logo_footer);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errLogoFooter').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo_footer);
            }

            if (size_foto < 2096103) {
                $('#errLogoFooter').html("");
                setToTrue(valid_logo_footer);
            }
        }else {
            $('#errLogoFooter').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_logo_footer);
        }
    });

    $('.logo_sidebar').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errLogoSidebar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo_sidebar);
            }

            if (size_foto < 2096103) {
                $('#errLogoSidebar').html("");
                setToTrue(valid_logo_sidebar);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errLogoSidebar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo_sidebar);
            }

            if (size_foto < 2096103) {
                $('#errLogoSidebar').html("");
                setToTrue(valid_logo_sidebar);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errLogoSidebar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_logo_sidebar);
            }

            if (size_foto < 2096103) {
                $('#errLogoSidebar').html("");
                setToTrue(valid_logo_sidebar);
            }
        }else {
            $('#errLogoSidebar').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_logo_sidebar);
        }
    });

    $('.icon').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errIcon').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_icon);
            }

            if (size_foto < 2096103) {
                $('#errIcon').html("");
                setToTrue(valid_icon);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errIcon').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_icon);
            }

            if (size_foto < 2096103) {
                $('#errIcon').html("");
                setToTrue(valid_icon);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errIcon').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_icon);
            }

            if (size_foto < 2096103) {
                $('#errIcon').html("");
                setToTrue(valid_icon);
            }
        }else {
            $('#errIcon').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_icon);
        }
    });

	$('#btn_submit').on('click',function(e){


		if ($('#name').val().length == 0) {
			setToFalse(valid_name);
		}else{
			setToTrue(valid_name);
		}

        if ($('#version').val().length == 0) {
			setToFalse(valid_version);
		}else{
			setToTrue(valid_version);
        }

        if ($('#tentang_kita').val().length == 0) {
			setToFalse(valid_tentang_kita);
		}else{
			setToTrue(valid_tentang_kita);
        }

        if ($('#sejarah').val().length == 0) {
			setToFalse(valid_sejarah);
		}else{
			setToTrue(valid_sejarah);
        }

        if ($('#email').val().length == 0) {
			setToFalse(valid_email);
		}else{
			setToTrue(valid_email);
        }

        if ($('#alamat').val().length == 0) {
			setToFalse(valid_alamat);
		}else{
			setToTrue(valid_alamat);
        }

        if ($('#no_hp').val().length == 0) {
			setToFalse(valid_no_hp);
		}else{
			setToTrue(valid_no_hp);
		}

        if ($('#meta_keyword').val().length == 0) {
			setToFalse(valid_meta_keyword);
		}else{
			setToTrue(valid_meta_keyword);
		}

        if ($('#meta_description').val().length == 0) {
			setToFalse(valid_meta_description);
		}else{
			setToTrue(valid_meta_description);
		}


		e.preventDefault();

        if (valid_name == true && valid_version == true && valid_tentang_kita == true
        && valid_sejarah == true && valid_email == true && valid_alamat == true
        && valid_no_hp == true && valid_logo == true && valid_gambar_tentang == true
        && valid_logo_header == true && valid_logo_footer == true && valid_logo_sidebar == true
        && valid_icon == true && valid_meta_keyword == true && valid_meta_description == true) {
			$('#form_master').submit();
		}else{
   			errMsg('Data masih belum terisi semua!');
		}
	});
</script>
@stop
