@if ($paginator->lastPage() > 1)
<ul>
    <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}"><a class="prev" href="{{ $paginator->url(1) }}"><i class="fas fa-arrow-left"></i> Prev</a></li>
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
    <li><a class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}" href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
    @endfor
    <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}"><a class="next" href="{{ $paginator->url($paginator->currentPage()+1) }}">Next <i class="fas fa-arrow-right"></i></a></li>
</ul>
@endif
