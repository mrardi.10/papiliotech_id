@extends('layouts.app_frontend')

@section('content')
  <!-- navbar -->
  <nav class="navbar fixed-top navbar-secondary navbar-expand-lg navbar-light">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('assets/frontend/images/PAPILIOtech-white.png') }}" class="img-fluid" alt="papiliotech"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fas fa-align-justify"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('testimonial') }}">Testimonials</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('blog') }}">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('project') }}">Portfolio</a>
                </li>
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown link
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li> -->
            </ul>
        </div>
    </div>
</nav>
</header>

 <!-- blog -->
 <main id="blog-list">
    <section id="blog-banner-head">
        <div class="container">
            <div class="row header-testimonials">
                <div class="col-lg wrapper">
                    <h1>Blog</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Blog</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <section id="blog-item">
        <div class="container">
            <div class="row">
                <div class="col-12 filter-nav-blog">
                    <ul class="nav categories justify-content-center">
                        <li class="nav-item">
                            <a id="btn-filter" data-filter="all" class="nav-link active" href="#">All</a>
                        </li>
                        @foreach ($catg as $item)
                        <li class="nav-item">
                            <a id="btn-filter" data-filter="{{ str_replace(' ','-',$item->categories) }}" class="nav-link" href="#">{{ $item->categories }}</a>
                            {{-- <a id="btn-filter" data-filter="{{ str_replace(' ','-',$item->categories) }}" class="nav-link" href="#">{{ $item->categories }}</a> --}}
                        </li>
                        @endforeach
                        {{-- <li class="nav-item">
                            <a class="nav-link active" href="#">Programming</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Technology</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Design</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Bussines</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">IOT</a>
                        </li> --}}
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row article justify-content-center blog-data">
                @if (count($blogs) > 0)
                @foreach ($blogs as $item)
                <div class="col-12 col-md-6 col-lg-4 col-xl-4 item-blog @foreach($categories as $data)@if($data->id_blog == $item->id){{ str_replace(' ','-',$data->categories) }}@endif @endforeach">
                    {{-- <div class="col-12 col-md-6 col-lg-4 col-xl-4 item-blog @foreach($categories as $data)@if($data->id == $item->id_blog){{ str_replace(' ','-',$data->categories) }}@endif @endforeach" data-aos="fade-up"> --}}
                    <div class="card">
                        <img src="{{ url('preview-file/foto-blog/'.$item->gambar) }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4><a href="{{ url('view-blog/'.$item->id) }}"> {{ $item->judul }}</a></h4>
                            <p class="card-text">@php echo $item->preview; @endphp </p>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"></li>
                            <li class="list-group-item d-flex justify-content-between">
                                <span class="category"> {{ $item->categories }}</span>
                                <span class="post-date">{{ Helper::bulan_tanggal_en($item->tanggal) }}, {{ date('Y',strtotime($item->tanggal)) }}</span>
                            </li>
                            <li class="list-group-item credit">
                                @if ($item->avatar != "")
                                <div class="ava"><img src="{{ url('preview-file/ava-users/'.$item->avatar) }}" class="img-fluid" alt=""></div>
                                @else
                                <div class="ava"><img src="{{ asset('assets/backend/plugins/dropify/not_found.png') }}" class="img-fluid" alt=""></div>
                                @endif

                                <span class="text-uppercase"> BY <b>{{ $item->name }}</b></span>
                            </li>
                        </ul>
                    </div>
                </div>
                @endforeach
                @else
                <div class="col-12 col-md-6 col-lg-4 col-xl-4" data-aos="fade-up">
                    <div class="card">
                        <img src="{{ asset('assets/frontend/images/blog/drew-beamer-Se7vVKzYxTI-unsplash.jpg') }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4><a href="detail-blog.html"> Lorem ipsum dolor sit amet consectetur.</a></h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the
                                bulk of the card's content.</p>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"></li>
                            <li class="list-group-item d-flex justify-content-between">
                                <span class="category">Design</span>
                                <span class="post-date">May 02, 2020</span>
                            </li>
                            <li class="list-group-item credit">
                                <div class="ava"><img src="images/tedy.jpg" class="img-fluid" alt=""></div>
                                <span> BY <b>TEDY HIDAYAT</b></span>
                            </li>
                        </ul>
                    </div>
                </div>
                @endif
                <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                <div class="col-lg pagination-wrapper">
                    <nav aria-label="pagination">
                        @include('blog_pagination', ['paginator' => $blogs])
                    </nav>
                </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection


