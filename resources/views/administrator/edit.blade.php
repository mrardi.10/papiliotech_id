@extends('layouts.app_backend')

@section('title')
Ubah Administrator
@stop

@section('title_page')
Ubah Administrator
@stop

@section('css_after')
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/dropify/dropify.min.css')}}">
@stop

@section('content')
<section class="content">
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Data Administrator</h3><br>
                <p>Pastikan yang merupakan tanda (*) diisi dengan benar</p>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="" method="post" id="form_master" enctype="multipart/form-data">
              	@csrf
                <div class="box-body">

                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                  			    <label for="nama">Nama <span class="wajib">*</span></label>
							    <input type="text" name="name" class="form-control" id="nama" value="{{ $data->name }}">
                  			    <span class="err" id="errNama"></span>
                            </div>
                            <div class="form-group">
                                <label for="jabatan">Email <span class="wajib">*</span></label>
                                <input type="email" name="email" class="form-control" id="email" value="{{ $data->email }}" autocomplete="off" readonly>
                                <span class="err" id="errEmail"></span>
                            </div>
                            {{-- <div class="form-group">
                                <label for="password">Password <span class="wajib">*</span></label>
                                <input type="password" name="password" class="form-control" id="password" value="{{ old('password') }}">
                                <span class="err" id="errPassword"></span>
                            </div>
                            <div class="form-group">
                                <label for="ulang_password">Ulang Password <span class="wajib">*</span></label>
                                <input type="password" name="ulang_password" class="form-control" id="ulang_password" value="{{ old('ulang_password') }}">
                                <span class="err" id="errUlangPassword"></span>
                            </div> --}}
                            <div class="form-group">
                                <label for="">Avatar <span class="wajib">*</span></label>
                                @if($data->avatar != "")
                                <input type="file" id="input-file-now-custom-1" class="dropify gambar" data-default-file="{{url('/')}}/storage/app/public/ava-users/{{ $data->avatar }}" name="gambar"/>
                                @else
                                <input type="file" id="input-file-now-custom-1" class="dropify gambar" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="gambar"/>
                                @endif
                                <input type="hidden" value="{{ $data->avatar }}" name="old_gambar">
								<span class="err" id="errGambar"></span>
                            </div>
						</div>
                  	</div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="button" class="btn bg-primary" id="btn_submit">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
</section>
@stop

@section('js_after')

<script src="{{ asset('assets/backend/plugins/dropify/dropify.min.js') }}"></script>
<script type="text/javascript">

	$(document).ready(function(){
        // Basic
        $('.dropify').dropify();
         // Translated
         $('.dropify-fr').dropify({
            messages: {
               default: 'Glissez-déposez un fichier ici ou cliquez',
               replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
               remove:  'Supprimer',
               error:   'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element){
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element){
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element){
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e){
            e.preventDefault();
            if (drDestroy.isDropified()) {
               drDestroy.destroy();
            } else {
               drDestroy.init();
            }
        })
	});

    var valid_name = new Boolean(false);
    var valid_email = new Boolean(false);
    // var valid_password = new Boolean(false);
    // var valid_c_password = new Boolean(false);
    @if($data->avatar != "")
    var valid_gambar = new Boolean(true);
    @else
    var valid_gambar = new Boolean(false);
    @endif

	$('#nama').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_name);
			$('#errNama').html("Nama tidak boleh Kosong");

		}else{
			$('#errNama').html("");
			setToTrue(valid_name);
		}
	});

    $('#email').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_email);
			$('#errEmail').html("Email tidak boleh Kosong");

		}else{
			$('#errEmail').html("");
			setToTrue(valid_email);
		}
	});

    // $('#password').on('keyup',function(){
	// 	if ($(this).val() == "") {
	// 		setToFalse(valid_password);
	// 		$('#errPassword').html("Password tidak boleh Kosong");

	// 	}else{
	// 		$('#errPassword').html("");
	// 		setToTrue(valid_password);
	// 	}
	// });

    // $('#password').on('keyup',function(){
	// 	if ($(this).val().length < 6) {
	// 		setToFalse(valid_password);
	// 		$('#errPassword').html("Password minimal 6 karakter");

	// 	}else{
	// 		$('#errPassword').html("");
	// 		setToTrue(valid_password);
	// 	}
	// });

    // $('#ulang_password').on('keyup',function(){
	// 	if ($(this).val() == "") {
	// 		setToFalse(valid_c_password);
	// 		$('#errUlangPassword').html("Ulang Password tidak boleh Kosong");

	// 	}else{
	// 		$('#errUlangPassword').html("");
	// 		setToTrue(valid_c_password);
	// 	}
	// });

    $('.gambar').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }else {
            $('#errGambar').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_gambar);
        }
    });

	$('#btn_submit').on('click',function(e){


		if ($('#nama').val().length == 0) {
			setToFalse(valid_name);
		}else{
			setToTrue(valid_name);
		}

        if ($('#email').val().length == 0) {
			setToFalse(valid_email);
		}else{
			setToTrue(valid_email);
		}


		e.preventDefault();

		if (valid_name == true && valid_email == true && valid_gambar == true) {
			$('#form_master').submit();
		}else{
   			errMsg('Data masih belum terisi semua!');
		}
	});
</script>
@stop
