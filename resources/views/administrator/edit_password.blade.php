@extends('layouts.app_backend')

@section('title')
Ubah Password
@stop

@section('title_page')
Ubah Password
@stop

@section('css_after')
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/dropify/dropify.min.css')}}">
@stop

@section('content')
<section class="content">
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Data Administrator</h3><br>
                {{-- <p>Pastikan yang merupakan tanda (*) diisi dengan benar</p> --}}
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="" method="post" id="form_master" enctype="multipart/form-data">
              	@csrf
                <div class="box-body">

                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                  			    <label for="current_password">Password lama <span class="wajib">*</span></label>
							    <input type="password" name="current_password" class="form-control" id="current_password" value="{{ old('current_password') }}">
                  			    <span class="err" id="errPassLama"></span>
                            </div>
                            <div class="form-group">
                                <label for="password">Password Baru <span class="wajib">*</span></label>
                                <input type="password" name="password" class="form-control" id="password" value="{{ old('password') }}">
                                <span class="err" id="errPassword"></span>
                            </div>
                                <div class="form-group">
                                <label for="current_password">Ulang Password Baru <span class="wajib">*</span></label>
                                <input type="password" name="c_password" class="form-control" id="ulang_password" value="{{ old('c_password') }}">
                                <span class="err" id="errUlangPassword"></span>
                            </div>
						</div>
                  	</div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="button" class="btn bg-primary" id="btn_submit">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
</section>
@stop

@section('js_after')

<script src="{{ asset('assets/backend/plugins/dropify/dropify.min.js') }}"></script>
<script type="text/javascript">


    var valid_curr_password = new Boolean(false);
    var valid_password = new Boolean(false);
    var valid_c_password = new Boolean(false);


    $('#current_password').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_curr_password);
			$('#errPassLama').html("Password Lama tidak boleh Kosong");

		}else{
			$('#errPassLama').html("");
			setToTrue(valid_curr_password);
		}
	});

    $('#password').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_password);
			$('#errPassword').html("Password tidak boleh Kosong");

		}else{
			$('#errPassword').html("");
			setToTrue(valid_password);
		}
	});

    $('#password').on('keyup',function(){
		if ($(this).val().length < 6) {
			setToFalse(valid_password);
			$('#errPassword').html("Password minimal 6 karakter");

		}else{
			$('#errPassword').html("");
			setToTrue(valid_password);
		}
	});

    $('#ulang_password').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_c_password);
			$('#errUlangPassword').html("Ulang Password tidak boleh Kosong");

		}else{
			$('#errUlangPassword').html("");
			setToTrue(valid_c_password);
		}
	});



	$('#btn_submit').on('click',function(e){

        if ($('#current_password').val().length == 0) {
			setToFalse(valid_curr_password);
		}else{
			setToTrue(valid_curr_password);
		}

        if ($('#password').val().length == 0) {
			setToFalse(valid_password);
		}else{
			setToTrue(valid_password);
		}

        if ($('#ulang_password').val().length == 0) {
			setToFalse(valid_c_password);
		}else{
			setToTrue(valid_c_password);
		}

		e.preventDefault();

		if (valid_curr_password == true && valid_password == true && valid_c_password == true) {
			$('#form_master').submit();
		}else{
   			errMsg('Data masih belum terisi semua!');
		}
	});
</script>
@stop
