@extends('layouts.app_backend')

@section('title')
Tambah project
@stop

@section('title_page')
Tambah project
@stop

@section('css_after')
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/dropify/dropify.min.css')}}">

  <!-- Page JS Plugins CSS -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/summernote/summernote-bs4.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/summernote/summernote.css') }}">

  <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('assets/backend/plugins/select2/css/select2.min.css')}}">
    {{-- <link rel="stylesheet" href="{{ asset('assets/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}"> --}}

@stop

@section('content')
<section class="content">
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Data Project</h3><br>
                <p>Pastikan yang merupakan tanda (*) diisi dengan benar</p>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="" method="post" id="form_master" enctype="multipart/form-data">
              	@csrf
                <div class="box-body">

                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                  			    <label for="project">Project <span class="wajib">*</span></label>
							    <input type="text" name="project" class="form-control" id="project" value="{{ old('project') }}">
                  			    <span class="err" id="errProject"></span>
                            </div>
                            <div class="form-group">
                                <label for="link">Link <span class="wajib">*</span></label>
                                <input type="text" name="link" class="form-control" id="link" value="{{ old('link') }}">
                                <span class="err" id="errLink"></span>
                            </div>
                            <div class="form-group">
                                <label for="">Gambar <span class="wajib">*</span></label>
                                <input type="file" id="input-file-now-custom-1" class="dropify gambar" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="gambar"/>
								<span class="err" id="errGambar"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="project">Deskripsi <span class="wajib">*</span></label>
                                <textarea name="deskripsi" id="deskripsi" class="form-control deskripsi" cols="30" rows="10"></textarea>
                                <span class="err" id="errDeskripsi"></span>
                          </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="project">Categories <span class="wajib">*</span></label>
                                <select name="categories[]" id="categories" class="form-control" multiple="true" data-placeholder="Pilih Categories">
                                    @foreach ($categories as $item)
                                        <option value="{{ $item->id }}">{{ $item->categories }}</option>
                                    @endforeach
                                </select>
                                <span class="err" id="errCategories"></span>
                          </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="project">Tags <span class="wajib">*</span></label>
                                <select name="tags[]" id="tags" class="form-control" multiple="true"  data-placeholder="Pilih Tags">
                                    @foreach ($categories as $item)
                                        <option value="{{ $item->id }}">{{ $item->categories }}</option>
                                    @endforeach
                                </select>
                                <span class="err" id="errtags"></span>
                          </div>
                        </div>
                  	</div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="button" class="btn bg-primary" id="btn_submit">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
</section>
@stop

@section('js_after')

<!-- Dropify -->
<script src="{{ asset('assets/backend/plugins/dropify/dropify.min.js') }}"></script>

<!-- Summernote -->
<script src="{{ asset('assets/backend/plugins/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/summernote/summernote.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('assets/backend/plugins/select2/js/select2.full.min.js')}}"></script>

<script type="text/javascript">


  $(document).ready(function() {
    $('.deskripsi').summernote({

      height: "250px",
      styleWithSpan: false,
    });
  });

  $("#categories").select2({
			// theme: "bootstrap",

		});

        $("#tags").select2({
			// theme: "bootstrap",

		});

	$(document).ready(function(){
        // Basic
        $('.dropify').dropify();
         // Translated
         $('.dropify-fr').dropify({
            messages: {
               default: 'Glissez-déposez un fichier ici ou cliquez',
               replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
               remove:  'Supprimer',
               error:   'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element){
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element){
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element){
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e){
            e.preventDefault();
            if (drDestroy.isDropified()) {
               drDestroy.destroy();
            } else {
               drDestroy.init();
            }
        })
	});

    var valid_project = new Boolean(false);
    var valid_deskripsi = new Boolean(false);
    var valid_gambar = new Boolean(false);

	$('#project').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_project);
			$('#errProject').html("Project tidak boleh Kosong");

		}else{
			$('#errProject').html("");
			setToTrue(valid_project);
		}
	});

    $('#deskripsi').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_deskripsi);
			$('#errDeskripsi').html("Deskripsi tidak boleh Kosong");

		}else{
			$('#errDeskripsi').html("");
			setToTrue(valid_deskripsi);
		}
	});

    $('.gambar').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }else {
            $('#errGambar').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_gambar);
        }
    });

	$('#btn_submit').on('click',function(e){


		if ($('#project').val().length == 0) {
			setToFalse(valid_project);
		}else{
			setToTrue(valid_project);
		}

        if ($('#deskripsi').val().length == 0) {
			setToFalse(valid_deskripsi);
		}else{
			setToTrue(valid_deskripsi);
		}


		e.preventDefault();

		if (valid_project == true && valid_deskripsi == true && valid_gambar == true) {
			$('#form_master').submit();
		}else{
   			errMsg('Data masih belum terisi semua!');
		}
	});
</script>
@stop
