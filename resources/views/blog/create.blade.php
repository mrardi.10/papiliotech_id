@extends('layouts.app_backend')

@section('title')
Tambah Blog
@stop

@section('title_page')
Tambah Blog
@stop

@section('css_after')
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/dropify/dropify.min.css')}}">

  <!-- Page JS Plugins CSS -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/summernote/summernote-bs4.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/summernote/summernote.css') }}">

  <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('assets/backend/plugins/select2/css/select2.min.css')}}">
    {{-- <link rel="stylesheet" href="{{ asset('assets/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}"> --}}

@stop

@section('content')
<section class="content">
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Data Blog</h3><br>
                <p>Pastikan yang merupakan tanda (*) diisi dengan benar</p>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="" method="post" id="form_master" enctype="multipart/form-data">
              	@csrf
                <div class="box-body">

                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                  			    <label for="judul">Judul <span class="wajib">*</span></label>
							    <input type="text" name="judul" class="form-control" id="judul" value="{{ old('judul') }}">
                  			    <span class="err" id="errJudul"></span>
                            </div>
                            <div class="form-group">
                                <label for="preview">Preview <span class="wajib">*</span></label>
                                <textarea name="preview" id="preview" class="form-control preview" cols="30" rows="10"></textarea>
                                <span class="err" id="errPreview"></span>
                            </div>
                            <div class="form-group">
                                <label for="">Gambar <span class="wajib">*</span></label>
                                <input type="file" id="input-file-now-custom-1" class="dropify gambar" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="gambar"/>
								<span class="err" id="errGambar"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="isi">Isi <span class="wajib">*</span></label>
                                <textarea name="isi" id="isi" class="form-control isi" cols="30" rows="10"></textarea>
                                <span class="err" id="errIsi"></span>
                          </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="categories">Category <span class="wajib">*</span></label>
                                <select name="categories[]" id="categories" class="form-control" data-placeholder="Pilih Categories">
                                    @foreach ($categories as $item)
                                        <option value="{{ $item->id }}">{{ $item->categories }}</option>
                                    @endforeach
                                </select>
                                <span class="err" id="errCategories"></span>
                          </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <select name="tags[]" id="tags" class="form-control" multiple="true"  data-placeholder="Pilih Tags">
                                    @foreach ($categories as $item)
                                        <option value="{{ $item->id }}">{{ $item->categories }}</option>
                                    @endforeach
                                </select>
                                <span class="err" id="errtags"></span>
                          </div>
                        </div>
                  	</div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="button" class="btn bg-primary" id="btn_submit">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
</section>
@stop

@section('js_after')

<!-- Dropify -->
<script src="{{ asset('assets/backend/plugins/dropify/dropify.min.js') }}"></script>

<!-- Summernote -->
<script src="{{ asset('assets/backend/plugins/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('assets/backend/plugins/summernote/summernote.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('assets/backend/plugins/select2/js/select2.full.min.js')}}"></script>

<script type="text/javascript">


  $(document).ready(function() {
    $('.isi').summernote({

      height: "250px",
      styleWithSpan: false,
    });
    $('.preview').summernote({
    height: "100px",
    styleWithSpan: false,
    });
  });

  $("#categories").select2({
			// theme: "bootstrap",

		});

        $("#tags").select2({
			// theme: "bootstrap",

		});

	$(document).ready(function(){
        // Basic
        $('.dropify').dropify();
         // Translated
         $('.dropify-fr').dropify({
            messages: {
               default: 'Glissez-déposez un fichier ici ou cliquez',
               replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
               remove:  'Supprimer',
               error:   'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element){
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element){
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element){
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e){
            e.preventDefault();
            if (drDestroy.isDropified()) {
               drDestroy.destroy();
            } else {
               drDestroy.init();
            }
        })
	});

    var valid_judul = new Boolean(false);
    var valid_preview = new Boolean(false);
    var valid_isi = new Boolean(false);
    var valid_gambar = new Boolean(false);
    var valid_categories = new Boolean(false);

	$('#judul').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_judul);
			$('#errJudul').html("Judul tidak boleh Kosong");

		}else{
			$('#errJudul').html("");
			setToTrue(valid_judul);
		}
	});

    $('#preview').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_preview);
			$('#errPreview').html("preview tidak boleh Kosong");

		}else{
			$('#errPreview').html("");
			setToTrue(valid_preview);
		}
	});

    $('#isi').on('keyup',function(){
        if ($(this).val() == "") {
            setToFalse(valid_isi);
            $('#errIsi').html("Isi tidak boleh Kosong");

        }else{
            $('#errIsi').html("");
            setToTrue(valid_isi);
        }
    });

    $('#categories').on('keyup',function(){
        if ($(this).val() == "") {
            setToFalse(valid_categories);
            $('#errCategories').html("Category tidak boleh Kosong");

        }else{
            $('#errCategories').html("");
            setToTrue(valid_categories);
        }
    });

    $('.gambar').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            // if(size_foto < 30720){
            //     $('#errGambar').html("file tidak boleh kurang dari 30kb");
            //     setToFalse(valid_gambar);
            // }

            if (size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }else {
            $('#errGambar').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_gambar);
        }
    });

	$('#btn_submit').on('click',function(e){


		if ($('#judul').val().length == 0) {
			setToFalse(valid_judul);
		}else{
			setToTrue(valid_judul);
		}

        if ($('#preview').val().length == 0) {
			setToFalse(valid_preview);
		}else{
			setToTrue(valid_preview);
		}

        if ($('#isi').val().length == 0) {
			setToFalse(valid_isi);
		}else{
			setToTrue(valid_isi);
		}

        if ($('#categories').val().length == 0) {
			setToFalse(valid_categories);
		}else{
			setToTrue(valid_categories);
		}


		e.preventDefault();

		if (valid_judul == true && valid_preview == true && valid_isi == true && valid_categories == true && valid_gambar == true) {
			$('#form_master').submit();
		}else{
   			errMsg('Data masih belum terisi semua!');
		}
	});
</script>
@stop
