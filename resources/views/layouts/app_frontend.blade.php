@php
    // use App\SetApps;
    use App\Clients;
    use App\Founder;
    use App\Menus;
    use App\Projects;
    use App\Services;
    use App\SetApps;
    use App\Testimonials;
    use App\Blogs;

    $apps = SetApps::first();

$menus = Menus::where('status','1')->orderBy('urutan','asc')->get();

$clients = Clients::where('status','1')->orderBy('urutan','asc')->get();

$services = Services::where('status','1')->orderBy('id','asc')->get();

$testimonials = Testimonials::orderBy('id','desc')->get();

$founder = Founder::orderBy('nama','asc')->get();

$project = Projects::orderBy('id','desc')->limit(6)->get();

$last_project = Projects::orderBy('id','desc')->limit(4)->get();

$last_blog = Blogs::orderBy('updated_at','desc')->limit(3)->get();

@endphp
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="{{ $apps->meta_keyword }}" />
    <meta name="description" content="{{ $apps->meta_description }}" />
    <title>{{ $apps->name }}</title>

    <link rel="icon" href="{{ url('preview-file/foto-web/'.$apps->icon) }}" type="image/png" sizes="16x16">
    <link rel="stylesheet" href="{{ asset('assets/frontend/libraries/owl.carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/libraries/aos/aos.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/frontend/libraries/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/libraries/venobox/venobox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/styles/main.css') }}">

    <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/toastr/toastr.min.css')}}">
    <!-- <link rel="stylesheet" href="libraries/bootstrap/css/bootstrap.min.css"> -->
    <!-- CSS only -->

    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Titillium+Web:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
</head>

<body>

   @yield('content')


<footer id="footer">
    <div class="container-fluid">
        <div class="row item-footer">
            <div class="col-12 col-md-4 col-lg-2 logo">
                <img src="{{ url('preview-file/foto-web/'.$apps->logo_footer) }}" alt="papiliotech">
            </div>
            <div class="col-12 col-md-4 col-lg-2">
                <h4>About Us</h4>
                <ul>
                    <li>@php
                        $kalimat_pertama = explode('.',$apps->tentang_kita);
                        echo   $kalimat_pertama[0];
                        @endphp</li>
                    <li class="mt-2">+62 {{ $apps->no_hp }}</li>
                    <li>{{ $apps->email }}</li>
                    <li class="mt-2">{{ $apps->alamat }}</li>
                </ul>
            </div>
            <div class="col-12 col-md-4 col-lg-2">
                <h4>Our Services</h4>
                <ul>
                    @if (count($services) > 0)
                        @foreach ($services as $item)
                            <li><a href="#">{{ $item->service }}</a></li>
                        @endforeach
                    @else
                    <li><a href="#">Web Design</a></li>
                    @endif
                </ul>
            </div>
            <div class="col-12 col-md-4 col-lg-2">
                <h4>Latest Project</h4>
                <ul>
                    @foreach ($last_project as $item)
                    <li><a href="{{ url('detail-project/'.$item->id) }}">{{ $item->project }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-12 col-md-4 col-lg-2">
                <h4>Blog Latest</h4>
                <ul>
                    @foreach ($last_blog as $item)
                    <li><a href="{{ url('view-blog/'.$item->id) }}">{{ $item->judul }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-12 col-md-4 col-lg-2">
                <h4>Social Media</h4>
                <ul>
                    <li><a href="{{ $apps->fb }}">Facebook</a></li>
                    <li><a href="{{ $apps->ig }}">Instagram</a></li>
                    <li><a href="{{ $apps->twitter }}">Twitter</a></li>
                    <li><a href="{{ $apps->linked_in }}">LinkedIn</a></li>
                </ul>
            </div>
        </div>
        <div class="row credit-footer">
            <div class="col-12 col-lg">
                &copy; {{ date('Y') }} <a href="#" class="site">www.papiliotech.com</a>
                <br />
                Made with &nbsp;<i class="fas fa-coffee text-warning"></i> <i class="fas fa-heart text-danger"></i> &nbsp;Allright Reserved
            </div>
            <div class="col-12 col-lg">
                <ul>
                    <li><a href="#">Sitemap</a></li>
                    <li><a href="#">FAQ</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

    <button class="btn-scrollTop">
        <i class="fas fa-chevron-up icon"></i>
        <small>TOP</small>
    </button>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>

    <!-- <script src="libraries/jquery/jquery.min.js"></script>
    <script src="libraries/bootstrap/js/bootstrap.min.js"></script> -->
    <script src="{{ asset('assets/frontend/libraries/fontawesome/js/all.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/libraries/aos/aos.js') }}"></script>
    <script src="{{ asset('assets/frontend/libraries/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/libraries/venobox/venobox.min.js') }}"></script>

    <!-- SweetAlert2 -->
    <script src="{{ asset('assets/backend/plugins/sweetalert2/sweetalert2.min.js')}} "></script>
    <!-- Toastr -->
    <script src="{{ asset('assets/backend/plugins/toastr/toastr.min.js')}}"></script>
    <script src="{{ asset('assets/backend/dist/js/js-function.js')}}"></script>

    <script>
        !(function($) {
            // Sidebar menu
            $(function(){
                $('#btn-sidemenu').on('click',function() {
                    $('#sidebar').css('left', '0');
                });
                $('#btn-closesidebar').on('click',function(){
                    $('#sidebar').removeAttr('style');
                });
            });

             // filter portfolio nav
             $('.filter-nav ul li a').on('click', function(e) {
                e.preventDefault();

                // button active nav filter
                $('.filter-nav ul li a').removeClass('active');
                $(this).addClass('active');

                // filter image portfolio
                let value = $(this).attr('data-filter');
                if(value == 'all') {
                    $('.portfolio-item .item').show('1000');
                } else {
                    $('.portfolio-item .item').not('.'+value).hide('3000');
                    $('.portfolio-item .item').filter('.'+value).show('3000');
                }

            });

            // filter blog nav
            $('.filter-nav-blog ul li a').on('click', function(e) {
                e.preventDefault();

                // button active nav filter
                $('.filter-nav-blog ul li a').removeClass('active');
                $(this).addClass('active');

                // filter image portfolio
                let value = $(this).attr('data-filter');
                if(value == 'all') {
                    $('.blog-data .item-blog').show('1000');
                } else {
                    $('.blog-data .item-blog').not('.'+value).hide('3000');
                    $('.blog-data .item-blog').filter('.'+value).show('3000');
                }

            });

            // scroll to top button
            $(window).scroll(function() {
                if ($(this).scrollTop() > 600) {
                    $('.btn-scrollTop').fadeIn('slow');
                } else {
                    $('.btn-scrollTop').fadeOut('slow');
                }
            });

            $('.btn-scrollTop').click(function() {
                $('html, body').animate({
                scrollTop: 0
                }, 1500, 'swing');
                return false;
            });

             // venobox
             $(window).on('load', function(e) {
                e.preventDefault();
                $(document).ready(function(e) {
                    // e.preventDefault();
                    $('.venobox').venobox({
                        share: [],
                        numeratio :true,
                        infinigall: true,
                        titlePosition: 'bottom',
                        border: '2px',
                        spinner: 'wave'
                    });
                });
            });

            // scroll menu sidebar
            $(function(){
                $('aside .page-scroll, #hero .page-scroll, .navbar .page-scroll').on('click',function(e) {

                    $('#sidebar ul li a').removeClass('active');
                    $(this).addClass('active');

                    var goto = $(this).attr('href');
                    var t = $(goto);
                    $('html').animate({
                        scrollTop: t.offset().top - 55
                    }, 1250, 'swing');
                    e.preventDefault();
                });

                @if(Session::has('msg'))
                @if (Session::get('msg') == 'captcha_null')
                $('#sidebar ul li a').removeClass('active');
                    $(this).addClass('active');

                    var goto = '#messages';
                    var t = $(goto);
                    $('html').animate({
                        scrollTop: t.offset().top - 55
                    }, 1000, 'swing');
                    e.preventDefault();
                @endif
                @endif
            });

            // owl carousel
            $(".box-clients").owlCarousel({
                autoplay: true,
                dots: false,
                loop: true,
                items: 6,
                margin: 8,
                nav: false,
                responsive: {
                    0: {
                        items: 3
                    },
                    600: {
                        items: 3
                    },
                    768: {
                        items: 5
                    },
                    1000: {
                        items: 6
                    }
                }
            });

            $(".box-testimonials").owlCarousel({
                autoplay: true,
                dots: false,
                loop: true,
                items: 6,
                margin: 8,
                nav: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    768: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            });

            /* aos animate */
            AOS.init({
                duration: 800,
                easing: "ease-in-out"
            });
        })(jQuery);

        $(function(){
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });

       @if(Session::has('msg'))
        Toast.fire({
        @if(Session::get('msg') == 'success_send')
          icon: 'success',
          title: 'Berhasil kirim pesan'
        @elseif(Session::get('msg') == 'fail_send')
          icon: 'error',
          title: 'Gagal kirim pesan'
          @elseif(Session::get('msg') == 'captcha_null')
          icon: 'error',
          title: 'Captcha tidak boleh kosong!'
        @endif
        })
       @endif

    })
    </script>
</body>

</html>
