@extends('layouts.app_backend')

@section('title')
Ubah Category
@stop

@section('title_page')
Ubah Category
@stop

@section('css_after')
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

@stop

@section('content')
<section class="content">
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Data Category</h3><br>
                <p>Pastikan yang merupakan tanda (*) diisi dengan benar</p>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="" method="post" id="form_master">
              	@csrf
                <div class="box-body">

                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                  			  <label for="categories">Category <span class="wajib">*</span></label>
							<input type="text" name="categories" class="form-control" id="categories" value="{{ $data->categories }}">
                  			  <span class="err" id="errCategories"></span>
                            </div>
						</div>
                  	</div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="button" class="btn bg-primary" id="btn_submit">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
</section>
@stop

@section('js_after')

<script type="text/javascript">

	$(document).ready(function(){

	});

    var valid_categories = new Boolean(false);

	$('#categories').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_categories);
			$('#errCategories').html("Category tidak boleh Kosong");

		}else{
			$('#errCategories').html("");
			setToTrue(valid_categories);
		}
	});

	$('#btn_submit').on('click',function(e){


		if ($('#categories').val().length == 0) {
			setToFalse(valid_categories);
		}else{
			setToTrue(valid_categories);
		}


		e.preventDefault();

		if (valid_categories == true) {
			$('#form_master').submit();
		}else{
   			errMsg('Data masih belum terisi semua!');
		}
	});
</script>
@stop
