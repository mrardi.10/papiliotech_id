@extends('layouts.app_backend')

@section('title')
Tambah Founder
@stop

@section('title_page')
Tambah Founder
@stop

@section('css_after')
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/backend/plugins/dropify/dropify.min.css')}}">
@stop

@section('content')
<section class="content">
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">Data Founder</h3><br>
                <p>Pastikan yang merupakan tanda (*) diisi dengan benar</p>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="" method="post" id="form_master" enctype="multipart/form-data">
              	@csrf
                <div class="box-body">

                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
                  			    <label for="nama">Nama <span class="wajib">*</span></label>
							    <input type="text" name="nama" class="form-control" id="nama" value="{{ old('nama') }}">
                  			    <span class="err" id="errNama"></span>
                            </div>
                            <div class="form-group">
                                <label for="jabatan">Jabatan <span class="wajib">*</span></label>
                                <input type="text" name="jabatan" class="form-control" id="jabatan" value="{{ old('jabatan') }}">
                                <span class="err" id="errJabatan"></span>
                            </div>
                            <div class="form-group">
                                <label for="">Gambar <span class="wajib">*</span></label>
                                <input type="file" id="input-file-now-custom-1" class="dropify gambar" data-default-file="{{url('/')}}/assets/backend/plugins/dropify/not_found.png" name="gambar"/>
								<span class="err" id="errGambar"></span>
                            </div>
						</div>
                  	</div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="button" class="btn bg-primary" id="btn_submit">Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
</section>
@stop

@section('js_after')

<script src="{{ asset('assets/backend/plugins/dropify/dropify.min.js') }}"></script>
<script type="text/javascript">

	$(document).ready(function(){
        // Basic
        $('.dropify').dropify();
         // Translated
         $('.dropify-fr').dropify({
            messages: {
               default: 'Glissez-déposez un fichier ici ou cliquez',
               replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
               remove:  'Supprimer',
               error:   'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element){
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element){
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element){
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e){
            e.preventDefault();
            if (drDestroy.isDropified()) {
               drDestroy.destroy();
            } else {
               drDestroy.init();
            }
        })
	});

    var valid_nama = new Boolean(false);
    var valid_jabatan = new Boolean(false);
    var valid_gambar = new Boolean(false);

	$('#nama').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_nama);
			$('#errNama').html("Nama tidak boleh Kosong");

		}else{
			$('#errNama').html("");
			setToTrue(valid_nama);
		}
	});

    $('#jabatan').on('keyup',function(){
		if ($(this).val() == "") {
			setToFalse(valid_jabatan);
			$('#errJabatan').html("Jabatan tidak boleh Kosong");

		}else{
			$('#errJabatan').html("");
			setToTrue(valid_jabatan);
		}
	});

    $('.gambar').bind('change', function() {
        var size_foto = this.files[0].size;
        var type_foto = this.files[0].type;

        if (type_foto == 'image/png') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            if(size_foto < 30720){
                $('#errGambar').html("file tidak boleh kurang dari 30kb");
                setToFalse(valid_gambar);
            }

            if (30720 < size_foto && size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            if(size_foto < 30720){
                $('#errGambar').html("file tidak boleh kurang dari 30kb");
                setToFalse(valid_gambar);
            }

            if (30720 < size_foto && size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }
        else if (type_foto == 'image/jpeg') {
            if(size_foto > 2096103){
                $('#errGambar').html("file tidak boleh lebih dari 2MB");
                setToFalse(valid_gambar);
            }

            if(size_foto < 30720){
                $('#errGambar').html("file tidak boleh kurang dari 30kb");
                setToFalse(valid_gambar);
            }

            if (30720 < size_foto && size_foto < 2096103) {
                $('#errGambar').html("");
                setToTrue(valid_gambar);
            }
        }else {
            $('#errGambar').html("type file tidak boleh selain jpg, jpeg, png");
            setToFalse(valid_gambar);
        }
    });

	$('#btn_submit').on('click',function(e){


		if ($('#nama').val().length == 0) {
			setToFalse(valid_nama);
		}else{
			setToTrue(valid_nama);
		}

        if ($('#jabatan').val().length == 0) {
			setToFalse(valid_jabatan);
		}else{
			setToTrue(valid_jabatan);
		}


		e.preventDefault();

		if (valid_nama == true && valid_jabatan == true && valid_gambar == true) {
			$('#form_master').submit();
		}else{
   			errMsg('Data masih belum terisi semua!');
		}
	});
</script>
@stop
