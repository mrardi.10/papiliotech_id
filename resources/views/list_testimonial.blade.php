@extends('layouts.app_frontend')

@section('content')

<!-- navbar -->
<nav class="navbar navbar-primary nav-alternate fixed-top">
    <a class="navbar-brand page-scroll" href="{{ url('/') }}"><img src="{{ asset('assets/frontend/images/PAPILIOtech..png') }}" alt=""></a>
    <ul class="nav btn-backtohome">
        <li class="nav-item text-center" id="btn-sidemenu">
            <a href="{{ url('/') }}" class="nav-link"><i class="fas fa-arrow-left icon"></i> <span>Home</span> </a>
        </li>
    </ul>
    <ul class="nav ml-auto btn-socmed">
        <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->twitter }}"><i class="fab fa-twitter"></i></a></li>
        <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->fb }}"><i class="fab fa-facebook-f"></i></a></li>
        <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->ig }}"><i class="fab fa-instagram"></i></a></li>
        <li class="nav-item"><a class="nav-link d-none d-sm-block" href="{{ $apps->linked_in }}"><i class="fab fa-linkedin"></i></a></li>
    </ul>
</nav>

<!-- testimonials list -->
<section id="testimonials-list">
    <div class="container-fluid">
        <div class="row header-testimonials">
            <div class="col-lg wrapper">
                <div class="title">
                    <h2>What client said in</h2>
                    <h1>Testimonials</h1>
                </div>
                <div class="item-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Testimonials</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row content-testimonials">
            <div class="col-lg">
                <div class="card-columns">
                    @if(count($testi) > 0)
                    @foreach ($testi as $item)
                    <div class="card" data-aos="fade-up">
                        <div class="card-header">
                            <div class="img-rounded">
                                <img src="{{ url('preview-file/foto-testimonials/'.$item->foto) }}">
                            </div>
                        </div>
                        <div class="card-body">
                            <h5>{{ $item->nama }}</h5>
                            <small>{{ $item->dari }}</small>
                            <p>
                                {{ $item->testi }}
                            </p>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="card" data-aos="fade-up">
                        <div class="card-header">
                            <div class="img-rounded">
                                <img src="{{ asset('assets/frontend/images/t2.jpg') }}">
                            </div>
                        </div>
                        <div class="card-body">
                            <h5>Albert Sneider</h5>
                            <small>MNC GROUP</small>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit inventore omnis.
                            </p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
