<ul class="sidebar-menu" data-widget="tree">

    <li class="{{ Helper::setChildActive(['dashboard']) }}">
        <a href="{{ url('dashboard') }}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>

    <li class="{{ Helper::setChildActive(['setting-web']) }}">
        <a href="{{ url('setting-web') }}">
          <i class="fa fa-dashboard"></i> <span>Setting Web</span>
        </a>
    </li>

    <li class="treeview {{ Helper::setParentActive([
        'data-menu','create-menu','edit-menu',
        'data-category','create-category','edit-category',
        'data-founder','create-founder','edit-founder'
    ]) }}">
        <a href="#">
          <i class="fa fa-archive"></i> <span>Master Data</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
            <li class="{{ Helper::setChildActive(['data-menu','create-menu','edit-menu']) }} hidden">
                <a href="{{ url('data-menu') }}">
                    <i class="fa fa-circle-o"></i> Data Menu
                </a>
            </li>
            <li class="{{ Helper::setChildActive(['data-administrator','create-administrator','edit-administrator']) }}">
                <a href="{{ url('data-administrator') }}">
                    <i class="fa fa-circle-o"></i> Data Admin
                </a>
            </li>
            <li class="{{ Helper::setChildActive(['data-category','create-category','edit-category']) }}">
                <a href="{{ url('data-category') }}">
                    <i class="fa fa-circle-o"></i> Data Category
                </a>
            </li>
            <li class="{{ Helper::setChildActive(['data-founder','create-founder','edit-founder']) }}">
                <a href="{{ url('data-founder') }}">
                    <i class="fa fa-circle-o"></i> Data Founder
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ Helper::setChildActive(['data-projects','create-projects','edit-projects','view-projects']) }}">
        <a href="{{ url('data-projects') }}">
          <i class="fa fa-dashboard"></i> <span>Projects</span>
        </a>
    </li>
    <li class="{{ Helper::setChildActive(['data-services','create-services','edit-services']) }}">
        <a href="{{ url('data-services') }}">
          <i class="fa fa-dashboard"></i> <span>Services</span>
        </a>
    </li>
    <li class="{{ Helper::setChildActive(['data-clients','create-clients','edit-clients']) }}">
        <a href="{{ url('data-clients') }}">
          <i class="fa fa-dashboard"></i> <span>Clients</span>
        </a>
    </li>
    <li class="{{ Helper::setChildActive(['data-messages','view-messages']) }}">
        <a href="{{ url('data-messages') }}">
          <i class="fa fa-dashboard"></i> <span>Messages</span>
        </a>
    </li>
    <li class="{{ Helper::setChildActive(['data-blog','create-blog','edit-blog','view-blog']) }}">
        <a href="{{ url('data-blog') }}">
          <i class="fa fa-dashboard"></i> <span>Blog</span>
        </a>
    </li>
    <li class="{{ Helper::setChildActive(['data-testimonials','create-testimonials','edit-testimonials','view-testimonials']) }}">
        <a href="{{ url('data-testimonials') }}">
          <i class="fa fa-dashboard"></i> <span>Testimonials</span>
        </a>
    </li>

  </ul>
