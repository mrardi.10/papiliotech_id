<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectCategories extends Model
{
    //
    protected $table = 'data_project_categories';
}
