<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategories extends Model
{
    //
    protected $table = 'data_blog_categories';
}
