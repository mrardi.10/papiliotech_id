<?php
namespace App\Helpers;

// use App\DataRole;
// use App\ModelAlamatKop;
// use App\ModelKopPDF;
// use App\ModelMenuSetting;
// use App\ModelNamaTtd;
// use App\ModelLog;
// use Illuminate\Http\Request;
use Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class User
{

    public static function tanggal_indo(string $tanggal, $cetak_hari = false)
    {
        $bulan = array(
            1 =>
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Agu',
            'Sep',
            'Okt',
            'Nov',
            'Des',
        );
        $hari = array(1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu',
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
        $anInt = intval($pecahkan[2]);
        $tgl_indo = $anInt . ' ' . $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
        if ($cetak_hari) {
            $num = date('N', strtotime($tanggal));
            return $hari[$num] . ', ' . $tgl_indo;
        }
        return $tgl_indo;
    }

    public static function bulan_tanggal_en(string $tanggal, $cetak_hari = false)
    {
        $bulan = array(
            1 =>
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
        );
        $hari = array(1 => 'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu',
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
        $anInt = intval($pecahkan[2]);
        $bulan_tanggal = $bulan[(int) $pecahkan[1]] . ' ' . $anInt  ;
        if ($cetak_hari) {
            $num = date('N', strtotime($tanggal));
            return $hari[$num] . ', ' . $bulan_tanggal;
        }
        return $bulan_tanggal;
    }

    public static function usia(string $tanggal){
        $age = 0;
        //explode the date to get month, day and year
        $birthDate = explode("-", $tanggal);
        //get age from date or birthdate
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date("md")
          ? ((date("Y") - $birthDate[0]) - 1) : (date("Y") - $birthDate[0]));

          return $age;
    }

public static function setChildActive($path, $active = 'active') {

    return call_user_func_array('Request::is', (array)$path) ? $active : '';

}

  public static function setParentActive( $path,$open = 'active menu-open')
{
    return call_user_func_array('Request::is', (array)$path) ? $open : '';


}








}
