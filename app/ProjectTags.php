<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTags extends Model
{
    //
    protected $table = 'data_project_tags';
}
