<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Blogs;
use App\BlogCategories;
use App\BlogTags;
use App\Categories;
use App\Founder;
use App\ProjectCategories;
use App\Projects;
use App\ProjectTags;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

class AdministratorController extends Controller
{
    //
    public function index(Request $request)
    {
        $role = Auth::user()->role;

         if ($role == 1){

      if($request->ajax())
      {
        DB::statement(DB::raw('set @rownum=0'));

        $data = DB::table('users')
        ->select('users.*',DB::raw('@rownum  := @rownum  + 1 AS rownum'))
        ->orderBy('id','desc')
        ->get();

    return Datatables::of($data)
            ->addColumn('avatar', '@if($avatar != "")
                <img src="{{ asset("storage/app/public/ava-users/".$avatar)  }}" alt="{{ $name }}" style="height:50px">
                    @else
                    <img src="{{ asset("assets/backend/dist/img/default/user.png")}}"  title="Foto" style="height:50px" alt="Foto"> @endif')
            ->addColumn('status','@if($status != 0) <span class="label bg-success">Yes</span>
            @else
            <span class="label bg-danger">No</span> @endif')
          ->addColumn('action',
          '<a href="{{ url("edit-administrator/".$id) }}">
            <button class="btn btn-sm bg-info white" data-toggle="tooltip" data-placement="left" title="Ubah"><i class="fa fa-edit"></i></button>
           </a>
           <a href="{{ url("edit-password-administrator/".$id) }}">
            <button class="btn btn-sm bg-warning white" data-toggle="tooltip" data-placement="left" title="Ubah"><i class="fa fa-lock"></i></button>
           </a>
        <a href="{{ url("change-status/".$id."/administrator") }}">
            <button class="btn btn-sm bg-danger white" data-toggle="tooltip" data-placement="left" title="Ubah Status">
                @if ($status == 1)
                <i class="fa fa-toggle-off"></i>
                @else
                <i class="fa fa-toggle-on"></i>
                @endif
            </button>
        </a>')
    ->rawColumns(['action','status','avatar'])
    ->make(true);
      }

      return view('administrator.data');


        }else{
            return abort(404);
        }
    }

    public function create()
    {

        return view('administrator.create');

    }

    public function store(Request $request)
    {

        $rules = array(
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:6',
            'ulang_password' => 'required|same:password',
            'gambar' => 'required|max:2048|mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('create-administrator')
                  ->withErrors($validator)
                  ->withInput();
        }

        $name = $request->input('name');
        $email = $request->input('email');
        $password = Hash::make( $request->input('password'));
        $status = 1;

        $lower = strtolower($name);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = New Admin();
        $data->name = $name;
        $data->email = $email;
        $data->password = $password;
        $data->status = $status;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/ava-users'); // root storage path
            $file->move($location,$file_name);
        } else {
            $file_name = null;
        }

        $data->avatar = $file_name;
        if ($data->save()) {

            return Redirect::to('data-administrator')->with('msg_input','success_create');
          }else{
            return Redirect::to('data-administrator')->with('msg_input','fail_create');
          }
    }

    public function edit($id)
    {

      $data = Admin::find($id);

      return view('administrator.edit',compact('data'));
    }

    public function editPassword($id)
    {

      $data = Admin::find($id);

      return view('administrator.edit_password',compact('data'));
    }

    public function update(Request $request,$id)
    {

        if($request->input('password') != ""){
			$rules = array( 'name' => 'required',
            'email' => 'required',
            'password' => 'min:6',
            'ulang_password' => 'same:password',
            'gambar' => 'max:2048|mimes:jpeg,jpg,png');
		} else {
            $rules = array(
                'name' => 'required',
            'email' => 'required',
            'gambar' => 'max:2048|mimes:jpeg,jpg,png',);
        }

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('edit-administrator/'.$id)
                  ->withErrors($validator)
                  ->withInput();
        }


        $name = $request->input('name');
        $email = $request->input('email');
        $password = Hash::make($request->input('password'));

        $lower = strtolower($name);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = Admin::find($id);
        $data->name = $name;
        $data->email = $email;
        $data->password = $password;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/ava-users'); // root storage path
            $file->move($location,$file_name);
            if ($request->input('old_gambar') != "") {
                unlink('storage/app/public/ava-users/'.$request->input('old_gambar'));
              }
        } else {
            $file_name = $request->input('old_gambar');
        }

        $data->avatar = $file_name;

        if ($data->save()) {

            return Redirect::to('data-administrator')->with('msg_input','success_edit');
          }else{
            return Redirect::to('data-administrator')->with('msg_input','fail_edit');
          }
    }

    public function updatePassword(Request $request,$id)
    {




        $password = Hash::make($request->input('password'));
        $c_password = Hash::make($request->input('c_password'));
        $curr_pass = $request->input('current_password');
        $pass = $request->input('password');
        $c_pass = $request->input('c_password');




        $data = Admin::find($id);
        if ($data) {
            if (Hash::check($curr_pass, $data->password)) {
                if ($pass == $curr_pass) {
                    return Redirect::to('edit-password-administrator/'.$id)->with('msg_pass','not_same_before')->withInput();
                }elseif ($pass == $c_pass) {
                    $data->password = $password;
                    $data->save();
                    return Redirect::to('data-administrator')->with('msg_pass','success');
                }else{
                    return Redirect::to('edit-password-administrator/'.$id)->with('msg_pass','not_same_conf')->withInput();
                }

            }else{
                return Redirect::to('edit-password-administrator/'.$id)->with('msg_pass','not_same')->withInput();
            }
        }else{
            return Redirect::to('edit-password-administrator/'.$id)->with('msg_pass','failed')->withInput();
        }

    }

    public function destroy($id)
    {

      $data = Blogs::find($id);

      if ($data) {

        if ($data->gambar != "") {
            if (file_exists(storage_path('app/public/foto-blog'.$data->gambar))) {
                unlink('storage/app/public/foto-blog/'.$data->gambar);
            }
        }

        BlogTags::where('id_blog',$id)->delete();
        BlogCategories::where('id_blog',$id)->delete();

        $data->delete();

        return Redirect::to('data-blog')->with('msg_input','success_delete');
      }else{
        return Redirect::to('data-blog')->with('msg_input','fail_delete');
      }
    }
}
