<?php

namespace App\Http\Controllers;

use App\Testimonials;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class TestimonialsController extends Controller
{
    //
    public function index(Request $request)
    {
        $role = Auth::user()->role;
         if ($role == 1){

      if($request->ajax())
      {
        DB::statement(DB::raw('set @rownum=0'));

      $data = DB::table('data_testimonials')
      ->select('id','nama','dari','testi','foto','status',DB::raw('@rownum  := @rownum  + 1 AS rownum'))
      ->orderBy('id','desc')
      ->get();

    return Datatables::of($data)
          ->addColumn('gambar', '@if($foto != "")
          <img src="{{ asset("storage/app/public/foto-testimonials/".$foto)  }}" alt="{{ $nama }}" style="height:50px">
            @else
            <img src="{{ asset("assets/backend/dist/img/default/user.png")}}"  title="Foto" width="100" height="100" alt="Foto"> @endif')
            ->addColumn('status', '@if($status != 0) <span class="label bg-success">Active</span>
          @else
          <span class="label bg-danger">Unctive</span> @endif')
          ->addColumn('action', '<a href="{{ url("edit-testimonials/".$id) }}">
          <button class="btn btn-sm bg-info white" data-toggle="tooltip" data-placement="left" title="Ubah"><i class="fa fa-edit"></i></button>
      </a>
      <a href="{{ url("change-status/".$id."/testimonial") }}">
      <button class="btn btn-sm bg-danger white" data-toggle="tooltip" data-placement="left" title="Ubah Status">
          @if ($status == 1)
          <i class="fa fa-toggle-off"></i>
          @else
          <i class="fa fa-toggle-on"></i>
          @endif
      </button>
      </a>
      ')
    ->rawColumns(['gambar','status','action'])
    ->make(true);
      }

      return view('testimonials.data');


        }else{
            return abort(404);
        }
    }

    public function create()
    {

      return view('testimonials.create');
    }

    public function store(Request $request)
    {

        $rules = array(
            'nama' => 'required',
            'dari' => 'required',
            'testi' => 'required',
            'gambar' => 'required|min:30|max:2048|mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('create-testimonials')
                  ->withErrors($validator)
                  ->withInput();
        }

        $nama = $request->input('nama');
        $dari = $request->input('dari');
        $testi = $request->input('testi');

        $lower = strtolower($nama);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = New Testimonials();
        $data->nama = $nama;
        $data->dari = $dari;
        $data->testi = $testi;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-testimonials'); // root storage path
            $file->move($location,$file_name);
          } else {
              $file_name = null;
          }

          $data->foto = $file_name;



        if ($data->save()) {
            return Redirect::to('data-testimonials')->with('msg_input','success_create');
          }else{
            return Redirect::to('data-testimonials')->with('msg_input','fail_create');
          }
    }

    public function edit($id)
    {

      $data = Testimonials::find($id);

      return view('testimonials.edit',compact('data'));
    }

    public function update(Request $request,$id)
    {

        $rules = array(
            'nama' => 'required',
            'dari' => 'required',
            'testi' => 'required',
            'gambar' => 'min:30|max:2048|mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('edit-testimonials/'.$id)
                  ->withErrors($validator)
                  ->withInput();
        }

        $nama = $request->input('nama');
        $dari = $request->input('dari');
        $testi = $request->input('testi');

        $lower = strtolower($nama);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = Testimonials::find($id);
        $data->nama = $nama;
        $data->dari = $dari;
        $data->testi = $testi;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-testimonials'); // root storage path
            $file->move($location,$file_name);
            if ($request->input('old_gambar') != "") {
              unlink('storage/app/public/foto-testimonials/'.$request->input('old_gambar'));
            }
          } else {
              $file_name = $request->input('old_gambar');
          }

          $data->foto = $file_name;



        if ($data->save()) {
            return Redirect::to('data-testimonials')->with('msg_input','success_edit');
          }else{
            return Redirect::to('data-testimonials')->with('msg_input','fail_edit');
          }
    }

    public function destroy($id)
    {

      $data = Testimonials::find($id);

      if ($data) {

        if ($data->foto != "") {
            if (file_exists(storage_path('app/public/foto-testimonials'.$data->foto))) {
                unlink('storage/app/public/foto-testimonials/'.$data->foto);
            }
        }
        $data->delete();

        return Redirect::to('data-testimonials')->with('msg_input','success_delete');
      }else{
        return Redirect::to('data-testimonials')->with('msg_input','fail_delete');
      }
    }
}
