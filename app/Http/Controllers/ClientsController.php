<?php

namespace App\Http\Controllers;

use App\Clients;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ClientsController extends Controller
{
    //
    public function index(Request $request)
    {
        $role = Auth::user()->role;
         if ($role == 1){

      if($request->ajax())
      {
        DB::statement(DB::raw('set @rownum=0'));

      $data = DB::table('data_clients')
      ->select('id','logo','client','link','urutan','status',DB::raw('@rownum  := @rownum  + 1 AS rownum'))
      ->orderBy('urutan','asc')
      ->get();

    return Datatables::of($data)
          ->addColumn('logo', '@if($logo != "")
          <img src="{{ asset("storage/app/public/logo-clients/".$logo)  }}" alt="{{ $client }}" style="height:50px">
            @else
            <img src="{{ asset("assets/backend/dist/img/default/user.png")}}"  title="Foto" width="100" height="100" alt="Foto"> @endif')
            ->addColumn('status', '@if($status != 0) <span class="label bg-success">Active</span>
          @else
          <span class="label bg-danger">Unctive</span> @endif')
          ->addColumn('action', '<a href="{{ url("edit-clients/".$id) }}">
          <button class="btn btn-sm bg-info white" data-toggle="tooltip" data-placement="left" title="Ubah"><i class="fa fa-edit"></i></button>
      </a>
      <a href="{{ url("change-status/".$id."/clients") }}">
        <button class="btn btn-sm bg-danger white" data-toggle="tooltip" data-placement="left" title="Ubah Status">
            @if ($status == 1)
            <i class="fa fa-toggle-off"></i>
            @else
            <i class="fa fa-toggle-on"></i>
            @endif
        </button>
    </a>
      ')
    ->rawColumns(['logo','status','action'])
    ->make(true);
      }

      return view('clients.data');


        }else{
            return abort(404);
        }
    }

    public function create()
    {

      return view('clients.create');
    }

    public function store(Request $request)
    {

        $rules = array(
            'client' => 'required',
            'link' => 'required',
            'urutan' => 'required',
            'status' => 'required',
            'gambar' => 'required|max:2048|mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('create-clients')
                  ->withErrors($validator)
                  ->withInput();
        }

        $client = $request->input('client');
        $link = $request->input('link');
        $urutan = $request->input('urutan');
        $status = $request->input('status');

        $lower = strtolower($client);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = New Clients();
        $data->client = $client;
        $data->link = $link;
        $cek_urutan = Clients::where('urutan',$urutan)->first();
        $last_urutan = Clients::latest('urutan')->first();
        if ($cek_urutan != "") {
            if ($last_urutan != "") {
                $urut = $last_urutan->urutan + 1;
            }else{
                $urut = 1;
            }
            $cek_urutan->urutan = $urut;
            $cek_urutan->save();
        }
        $data->urutan = $urutan;
        $data->status = $status;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/logo-clients'); // root storage path
            $file->move($location,$file_name);
          } else {
              $file_name = null;
          }

          $data->logo = $file_name;



        if ($data->save()) {
            return Redirect::to('data-clients')->with('msg_input','success_create');
          }else{
            return Redirect::to('data-clients')->with('msg_input','fail_create');
          }
    }

    public function edit($id)
    {

      $data = Clients::find($id);

      return view('clients.edit',compact('data'));
    }

    public function update(Request $request,$id)
    {

        $rules = array(
            'client' => 'required',
            'link' => 'required',
            'urutan' => 'required',
            'status' => 'required',
            'gambar' => 'max:2048|mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('edit-clients/'.$id)
                  ->withErrors($validator)
                  ->withInput();
        }

        $client = $request->input('client');
        $link = $request->input('link');
        $urutan = $request->input('urutan');
        $status = $request->input('status');

        $lower = strtolower($client);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = Clients::find($id);
        $data->client = $client;
        $data->link = $link;
        $cek_urutan = Clients::where('urutan',$urutan)->first();
        if ($cek_urutan != "") {
            $cek_urutan->urutan = $data->urutan;
            $cek_urutan->save();
        }
        $data->urutan = $urutan;
        $data->status = $status;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/logo-clients'); // root storage path
            $file->move($location,$file_name);
            if ($request->input('old_gambar') != "") {
              unlink('storage/app/public/logo-clients/'.$request->input('old_gambar'));
            }
          } else {
              $file_name = $request->input('old_gambar');
          }

          $data->logo = $file_name;



        if ($data->save()) {
            return Redirect::to('data-clients')->with('msg_input','success_edit');
          }else{
            return Redirect::to('data-clients')->with('msg_input','fail_edit');
          }
    }

}
