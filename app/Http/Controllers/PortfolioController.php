<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Founder;
use App\ProjectCategories;
use App\Projects;
use App\ProjectTags;
use App\SetApps;
use App\Testimonials;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class PortfolioController extends Controller
{
    //

    public function list()
    {

        $project = Projects::get();
        $categories = ProjectCategories::join('data_categories','data_categories.id','=','data_project_categories.id_categories')->get();
        $tags = ProjectTags::join('data_categories','data_categories.id','=','data_project_tags.id_tags')->get();
        $catg = Categories::get();
        $apps = SetApps::first();


      return view('list_project',compact('project','categories','tags','apps','catg'));

    }

    public function testimonial()
    {

        $testi = Testimonials::where('status','1')->get();
        $apps = SetApps::first();


      return view('list_testimonial',compact('testi','apps'));

    }

    public function view($id)
    {

        $data = Projects::find($id);
        $categories = ProjectCategories::join('data_categories','data_categories.id','=','data_project_categories.id_categories')
        ->where('id_project',$id)->get();
        $tags = ProjectTags::join('data_categories','data_categories.id','=','data_project_tags.id_tags')
        ->where('id_project',$id)->get();

        $apps = SetApps::first();


      return view('detail_project',compact('data','categories','tags','apps'));

    }


}
