<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Founder;
use App\ProjectCategories;
use App\Projects;
use App\ProjectTags;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ProjectsController extends Controller
{
    //
    public function index(Request $request)
    {
        $role = Auth::user()->role;
         if ($role == 1){

      if($request->ajax())
      {
        DB::statement(DB::raw('set @rownum=0'));

      $data = DB::table('data_projects')
      ->select('id','project',DB::raw('@rownum  := @rownum  + 1 AS rownum'))
      ->orderBy('id','desc')
      ->get();

    return Datatables::of($data)
          ->addColumn('action', '<a href="{{ url("edit-projects/".$id) }}">
          <button class="btn btn-sm bg-info white" data-toggle="tooltip" data-placement="left" title="Ubah"><i class="fa fa-edit"></i></button>
      </a>
      <a href="{{ url("delete-projects/".$id) }}">
          <button class="btn btn-sm bg-danger white" title="hapus"><i class="fa fa-trash"></i></button>
      </a>
      ')
    ->rawColumns(['action'])
    ->make(true);
      }

      return view('projects.data');


        }else{
            return abort(404);
        }
    }

    public function create()
    {

        $categories = Categories::get();

      return view('projects.create',compact('categories'));
    }

    public function store(Request $request)
    {

        $rules = array(
            'project' => 'required',
            'deskripsi' => 'required',
            'link' => 'nullable',
            'gambar' => 'required|max:2048|mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('create-projects')
                  ->withErrors($validator)
                  ->withInput();
        }

        $project = $request->input('project');
        $deskripsi = $request->input('deskripsi');
        $link = $request->input('link');
        $tags = $request->input('tags');
        $categories = $request->input('categories');
        $n_tags = count($tags);
        $n_categories = count($categories);

        $lower = strtolower($project);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = New Projects();
        $data->project = $project;
        $data->deskripsi = $deskripsi;
        $data->link = $link;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-projects'); // root storage path
            $file->move($location,$file_name);
        } else {
              $file_name = null;
        }

          $data->gambar = $file_name;
        if ($data->save()) {

            if ($n_tags > 0) {
                for ($i=0; $i < $n_tags; $i++) {
                    $data_tag = New ProjectTags();
                    $data_tag->id_project = $data->id;
                    $data_tag->id_tags = $tags[$i];
                    $data_tag->save();
                }
            }

            if ($n_categories > 0) {
                for ($i=0; $i < $n_categories; $i++) {
                    $data_categories = New ProjectCategories();
                    $data_categories->id_project = $data->id;
                    $data_categories->id_categories = $categories[$i];
                    $data_categories->save();
                }
            }

            return Redirect::to('data-projects')->with('msg_input','success_create');
          }else{
            return Redirect::to('data-projects')->with('msg_input','fail_create');
          }
    }

    public function edit($id)
    {

      $data = Projects::find($id);

      $tags = ProjectTags::where('id_project',$id)->get();
      $tag = "[";
      $n_tag = count($tags);
      $n = 1;

      foreach ($tags as $key => $value) {
          $tag.='"'.$value->id_tags.'"';
          if ($n == $n_tag) {
              $tag.= "]";
          }else{
            $tag.= ",";
          }
          $n++;
      }

      $categories = ProjectCategories::where('id_project',$id)->get();
      $category = "[";
      $n_catg = count($categories);
      $n = 1;
      foreach ($categories as $key => $value) {
        $category.='"'.$value->id_categories.'"';
        if ($n == $n_catg) {
            $category.= "]";
        }else{
          $category.= ",";
        }
        $n++;
    }

      $catg = Categories::get();

      return view('projects.edit',compact('data','tag','category','catg'));
    }

    public function update(Request $request,$id)
    {

        $rules = array(
            'project' => 'required',
            'deskripsi' => 'required',
            'link' => 'nullable',
            'gambar' => 'max:2048|mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('edit-projects/'.$id)
                  ->withErrors($validator)
                  ->withInput();
        }


        $project = $request->input('project');
        $deskripsi = $request->input('deskripsi');
        $link = $request->input('link');
        $tags = $request->input('tags');
        $categories = $request->input('categories');
        $n_tags = count($tags);
        $n_categories = count($categories);

        $lower = strtolower($project);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = Projects::find($id);
        $data->project = $project;
        $data->deskripsi = $deskripsi;
        $data->link = $link;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-projects'); // root storage path
            $file->move($location,$file_name);
            if ($request->input('old_gambar') != "") {
              unlink('storage/app/public/foto-projects/'.$request->input('old_gambar'));
            }
          } else {
              $file_name = $request->input('old_gambar');
          }

          $data->gambar = $file_name;

        if ($data->save()) {

            ProjectTags::where('id_project',$id)->delete();
            ProjectCategories::where('id_project',$id)->delete();

            if ($n_tags > 0) {
                for ($i=0; $i < $n_tags; $i++) {
                    $data_tag = New ProjectTags();
                    $data_tag->id_project = $data->id;
                    $data_tag->id_tags = $tags[$i];
                    $data_tag->save();
                }
            }

            if ($n_categories > 0) {
                for ($i=0; $i < $n_categories; $i++) {
                    $data_categories = New ProjectCategories();
                    $data_categories->id_project = $data->id;
                    $data_categories->id_categories = $categories[$i];
                    $data_categories->save();
                }
            }

            return Redirect::to('data-projects')->with('msg_input','success_edit');
          }else{
            return Redirect::to('data-projects')->with('msg_input','fail_edit');
          }
    }

    public function destroy($id)
    {

      $data = Projects::find($id);

      if ($data) {

        if ($data->gambar != "") {
            if (file_exists(storage_path('app/public/foto-projects'.$data->gambar))) {
                unlink('storage/app/public/foto-projects/'.$data->gambar);
            }
        }

        ProjectTags::where('id_project',$id)->delete();
        ProjectCategories::where('id_project',$id)->delete();

        $data->delete();

        return Redirect::to('data-projects')->with('msg_input','success_delete');
      }else{
        return Redirect::to('data-projects')->with('msg_input','fail_delete');
      }
    }
}
