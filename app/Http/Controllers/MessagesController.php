<?php

namespace App\Http\Controllers;

use App\Messages;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class MessagesController extends Controller
{
    //
    public function index(Request $request)
    {
        $role = Auth::user()->role;
         if ($role == 1){

      if($request->ajax())
      {
        DB::statement(DB::raw('set @rownum=0'));

      $data = DB::table('data_messages')
      ->select('id','full_name','email','phone_number','message','status',DB::raw('@rownum  := @rownum  + 1 AS rownum'))
      ->orderBy('id','desc')
      ->get();

    return Datatables::of($data)
         ->addColumn('status', '@if($status != 0) <span class="label bg-danger">Deliverd</span>
          @else
          <span class="label bg-success">Read</span> @endif')
          ->addColumn('action', '

            <a href="{{ url("view-messages/".$id) }}">
            <button class="btn btn-sm bg-info white" data-toggle="tooltip" data-placement="left" title="View Message">
                View Message
            </button>
            </a>')
    ->rawColumns(['status','action'])
    ->make(true);
      }

      return view('messages.data');


        }else{
            return abort(404);
        }
    }

    public function create()
    {

      return view('clients.create');
    }

    public function store(Request $request)
    {

        if ($request->input('g-recaptcha-response') == "") {
            return Redirect::to('/')
            ->withInput()
            ->with('msg','captcha_null');
        }

        $rules = array(
            'full_name' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'message' => 'required',
            'g-recaptcha-response' => 'required|captcha',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::back()
                  ->withErrors($validator)
                  ->withInput()
                  ->with('msg','fail_send');
        }

        $full_name = $request->input('full_name');
        $phone_number = $request->input('phone_number');
        $email = $request->input('email');
        $message = $request->input('message');

        $data = New Messages();
        $data->full_name = $full_name;
        $data->phone_number = $phone_number;
        $data->email = $email;
        $data->message = $message;
        $data->status = 1;

        if ($data->save()) {

            // dd($data);
            return Redirect::back()->with('msg','success_send');
          }else{
            // dd($data);
            return Redirect::back()->with('msg','fail_send');
          }
    }

    public function view($id)
    {

      $data = Messages::find($id);

      return view('messages.view',compact('data','id'));
    }

}
