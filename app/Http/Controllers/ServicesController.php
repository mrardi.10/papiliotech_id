<?php

namespace App\Http\Controllers;

use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ServicesController extends Controller
{
    //
    public function index(Request $request)
    {
        $role = Auth::user()->role;
         if ($role == 1){

      if($request->ajax())
      {
        DB::statement(DB::raw('set @rownum=0'));

      $data = DB::table('data_services')
      ->select('id','service','deskripsi','gambar','status',DB::raw('@rownum  := @rownum  + 1 AS rownum'))
      ->orderBy('id','desc')
      ->get();

    return Datatables::of($data)
          ->addColumn('gambar', '@if($gambar != "")
          <img src="{{ asset("storage/app/public/foto-services/".$gambar)  }}" alt="{{ $service }}" style="height:50px">
            @else
            <img src="{{ asset("assets/backend/dist/img/default/user.png")}}"  title="Foto" width="100" height="100" alt="Foto"> @endif')
            ->addColumn('status', '@if($status != 0) <span class="label bg-success">Active</span>
          @else
          <span class="label bg-danger">Unctive</span> @endif')
          ->addColumn('action', '<a href="{{ url("edit-services/".$id) }}">
          <button class="btn btn-sm bg-info white" data-toggle="tooltip" data-placement="left" title="Ubah"><i class="fa fa-edit"></i></button>
      </a>
      <a href="{{ url("change-status/".$id."/services") }}">
        <button class="btn btn-sm bg-danger white" data-toggle="tooltip" data-placement="left" title="Ubah Status">
            @if ($status == 1)
            <i class="fa fa-toggle-off"></i>
            @else
            <i class="fa fa-toggle-on"></i>
            @endif
        </button>
    </a>
      ')
    ->rawColumns(['gambar','status','action'])
    ->make(true);
      }

      return view('services.data');


        }else{
            return abort(404);
        }
    }

    public function create()
    {

      return view('services.create');
    }

    public function store(Request $request)
    {

        $rules = array(
            'service' => 'required',
            'deskripsi' => 'required',
            'status' => 'required',
            'gambar' => 'required|max:2048|mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('create-services')
                  ->withErrors($validator)
                  ->withInput();
        }

        $service = $request->input('service');
        $deskripsi = $request->input('deskripsi');
        $status = $request->input('status');


        $lower = strtolower($service);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            if ($ganti == "ui/ux") {
                $foto = "ui-ux";
            }else {
                $foto = $ganti;
            }
        }else{
            $foto = $lower;
        }

        $data = New Services();
        $data->service = $service;
        $data->deskripsi = $deskripsi;
        $data->status = $status;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-services'); // root storage path
            $file->move($location,$file_name);
          } else {
              $file_name = null;
          }

          $data->gambar = $file_name;



        if ($data->save()) {
            return Redirect::to('data-services')->with('msg_input','success_create');
          }else{
            return Redirect::to('data-services')->with('msg_input','fail_create');
          }
    }

    public function edit($id)
    {

      $data = Services::find($id);

      return view('services.edit',compact('data'));
    }

    public function update(Request $request,$id)
    {

        $rules = array(
            'service' => 'required',
            'deskripsi' => 'required',
            'status' => 'required',
            'gambar' => 'max:2048|mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('edit-services/'.$id)
                  ->withErrors($validator)
                  ->withInput();
        }

        $service = $request->input('service');
        $deskripsi = $request->input('deskripsi');
        $status = $request->input('status');

        $lower = strtolower($service);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            if ($ganti == "ui/ux") {
                $foto = "ui-ux";
            }else {
                $foto = $ganti;
            }

        }else{
            $foto = $lower;
        }

        $data = Services::find($id);
        $data->service = $service;
        $data->deskripsi = $deskripsi;
        $data->status = $status;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-services'); // root storage path
            $file->move($location,$file_name);
            if ($request->input('old_gambar') != "") {
              unlink('storage/app/public/foto-services/'.$request->input('old_gambar'));
            }
          } else {
              $file_name = $request->input('old_gambar');
          }

          $data->gambar = $file_name;



        if ($data->save()) {
            return Redirect::to('data-services')->with('msg_input','success_edit');
          }else{
            return Redirect::to('data-services')->with('msg_input','fail_edit');
          }
    }

}
