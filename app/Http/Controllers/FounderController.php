<?php

namespace App\Http\Controllers;

use App\Founder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class FounderController extends Controller
{
    //
    public function index(Request $request)
    {
        $role = Auth::user()->role;
         if ($role == 1){

      if($request->ajax())
      {
        DB::statement(DB::raw('set @rownum=0'));

      $data = DB::table('data_founders')
      ->select('id','nama','jabatan','gambar',DB::raw('@rownum  := @rownum  + 1 AS rownum'))
      ->orderBy('id','desc')
      ->get();

    return Datatables::of($data)
          ->addColumn('gambar', '@if($gambar != "")
          <img src="{{ asset("storage/app/public/foto-founder/".$gambar)  }}" alt="{{ $nama }}" style="height:50px">
            @else
            <img src="{{ asset("assets/backend/dist/img/default/user.png")}}"  title="Foto" width="100" height="100" alt="Foto"> @endif')
          ->addColumn('action', '<a href="{{ url("edit-founder/".$id) }}">
          <button class="btn btn-sm bg-info white" data-toggle="tooltip" data-placement="left" title="Ubah"><i class="fa fa-edit"></i></button>
      </a>
      <a href="{{ url("delete-founder/".$id) }}">
          <button class="btn btn-sm bg-danger white" title="hapus"><i class="fa fa-trash"></i></button>
      </a>
      ')
    ->rawColumns(['gambar','action'])
    ->make(true);
      }

      return view('founders.data');


        }else{
            return abort(404);
        }
    }

    public function create()
    {

      return view('founders.create');
    }

    public function store(Request $request)
    {

        $rules = array(
            'nama' => 'required',
            'jabatan' => 'required',
            'gambar' => 'required|min:30|max:2048|mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('create-founder')
                  ->withErrors($validator)
                  ->withInput();
        }

        $nama = $request->input('nama');
        $jabatan = $request->input('jabatan');

        $lower = strtolower($nama);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = New Founder();
        $data->nama = $nama;
        $data->jabatan = $jabatan;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$jabatan."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-founder'); // root storage path
            $file->move($location,$file_name);
          } else {
              $file_name = null;
          }

          $data->gambar = $file_name;



        if ($data->save()) {
            return Redirect::to('data-founder')->with('msg_input','success_create');
          }else{
            return Redirect::to('data-founder')->with('msg_input','fail_create');
          }
    }

    public function edit($id)
    {

      $data = Founder::find($id);

      return view('founders.edit',compact('data'));
    }

    public function update(Request $request,$id)
    {

        $rules = array(
            'nama' => 'required',
            'jabatan' => 'required',
            'gambar' => 'min:30|max:2048|mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('edit-founder/'.$id)
                  ->withErrors($validator)
                  ->withInput();
        }

        $nama = $request->input('nama');
        $jabatan = $request->input('jabatan');

        $lower = strtolower($nama);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = Founder::find($id);
        $data->nama = $nama;
        $data->jabatan = $jabatan;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$jabatan."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-founder'); // root storage path
            $file->move($location,$file_name);
            if ($request->input('old_gambar') != "") {
              unlink('storage/app/public/foto-founder/'.$request->input('old_gambar'));
            }
          } else {
              $file_name = $request->input('old_gambar');
          }

          $data->gambar = $file_name;



        if ($data->save()) {
            return Redirect::to('data-founder')->with('msg_input','success_edit');
          }else{
            return Redirect::to('data-founder')->with('msg_input','fail_edit');
          }
    }

    public function destroy($id)
    {

      $data = Founder::find($id);

      if ($data) {

        if ($data->gambar != "") {
            if (file_exists(storage_path('app/public/foto-founder'.$data->gambar))) {
                unlink('storage/app/public/foto-founder/'.$data->gambar);
            }
        }
        $data->delete();

        return Redirect::to('data-founder')->with('msg_input','success_delete');
      }else{
        return Redirect::to('data-founder')->with('msg_input','fail_delete');
      }
    }
}
