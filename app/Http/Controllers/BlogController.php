<?php

namespace App\Http\Controllers;

use App\Blogs;
use App\BlogCategories;
use App\BlogTags;
use App\Categories;
use App\Founder;
use App\ProjectCategories;
use App\Projects;
use App\ProjectTags;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class BlogController extends Controller
{
    //
    public function index(Request $request)
    {
        $role = Auth::user()->role;

         if ($role == 1){

      if($request->ajax())
      {
        DB::statement(DB::raw('set @rownum=0'));

        $data = DB::table('data_blogs')
        ->join('users','users.id','=','data_blogs.id_created','left')
        ->join('data_blog_categories','data_blog_categories.id_blog','=','data_blogs.id','left')
        ->join('data_categories','data_categories.id','=','data_blog_categories.id_categories','left')
        ->select('data_blogs.id','judul','data_categories.categories as category','tanggal','publish','users.name as created',DB::raw('@rownum  := @rownum  + 1 AS rownum'))
        ->orderBy('data_blogs.id','desc')
        ->get();

    return Datatables::of($data)
          ->addColumn('category','@php echo $category @endphp')
          ->addColumn('tanggal','<input type="hidden" value="{{ $tanggal }}"> {{ Helper::tanggal_indo($tanggal) }}')
          ->addColumn('publish','@if($publish != 0) <span class="label bg-success">Yes</span>
          @else
          <span class="label bg-danger">No</span> @endif')
          ->addColumn('action',
          '<a href="{{ url("edit-blog/".$id) }}">
            <button class="btn btn-sm bg-info white" data-toggle="tooltip" data-placement="left" title="Ubah"><i class="fa fa-edit"></i></button>
           </a>
           <a href="{{ url("view-blog/".$id) }}">
            <button class="btn btn-sm bg-primary white" data-toggle="tooltip" data-placement="left" title="View Blog">
                View Blog
            </button>
           </a>
        <a href="{{ url("change-status/".$id."/blog") }}">
            <button class="btn btn-sm bg-danger white" data-toggle="tooltip" data-placement="left" title="Ubah Publish">
                @if ($publish == 1)
                <i class="fa fa-toggle-off"></i>
                @else
                <i class="fa fa-toggle-on"></i>
                @endif
            </button>
        </a>')
    ->rawColumns(['action','category','tanggal','publish'])
    ->make(true);
      }

      return view('blog.data');


        }else{
            return abort(404);
        }
    }

    public function create()
    {

        $categories = Categories::get();

        return view('blog.create',compact('categories'));

    }

    public function store(Request $request)
    {

        $rules = array(
            'judul' => 'required',
            'preview' => 'required',
            'isi' => 'required',
            'gambar' => 'required|max:2048|mimes:jpeg,jpg,png',
            'categories' => 'required'
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('create-blog')
                  ->withErrors($validator)
                  ->withInput();
        }

        $judul = $request->input('judul');
        $preview = $request->input('preview');
        $tags = $request->input('tags');
        $categories = $request->input('categories');

        $isi = $request->input('isi');
        $id_created = Auth::user()->id;

        if ($tags != "") {
            $n_tags = count($tags);
        }else{
            $n_tags = 0;
        }

        $n_categories = count($categories);

        $lower = strtolower($judul);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = New Blogs();
        $data->judul = $judul;
        $data->preview = $preview;
        $data->isi = $isi;
        $data->tanggal = Carbon::now();
        $data->id_created = $id_created;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-blog'); // root storage path
            $file->move($location,$file_name);
        } else {
            $file_name = null;
        }

        $data->gambar = $file_name;
        if ($data->save()) {

            if ($n_tags > 0) {
                for ($i=0; $i < $n_tags; $i++) {
                    $data_tag = New BlogTags();
                    $data_tag->id_blog = $data->id;
                    $data_tag->id_tags = $tags[$i];
                    $data_tag->save();
                }
            }

            if ($n_categories > 0) {
                for ($i=0; $i < $n_categories; $i++) {
                    $data_categories = New BlogCategories();
                    $data_categories->id_blog = $data->id;
                    $data_categories->id_categories = $categories[$i];
                    $data_categories->save();
                }
            }

            return Redirect::to('data-blog')->with('msg_input','success_create');
          }else{
            return Redirect::to('data-blog')->with('msg_input','fail_create');
          }
    }

    public function edit($id)
    {

      $data = Blogs::find($id);

      $tags = BlogTags::where('id_blog',$id)->get();


      $n_tag = count($tags);
      if ($n_tag > 0) {
        $tag = "[";
      }else{
          $tag = "[]";
      }
      $n = 1;

      foreach ($tags as $key => $value) {
          $tag.='"'.$value->id_tags.'"';
          if ($n == $n_tag) {
              $tag.= "]";
          }else{
            $tag.= ",";
          }
          $n++;
      }

      $categories = BlogCategories::where('id_blog',$id)->get();
      $category = "[";
      $n_catg = count($categories);
      $n = 1;
      foreach ($categories as $key => $value) {
        $category.='"'.$value->id_categories.'"';
        if ($n == $n_catg) {
            $category.= "]";
        }else{
          $category.= ",";
        }
        $n++;
    }

      $catg = Categories::get();

      return view('blog.edit',compact('data','tag','category','catg'));
    }

    public function update(Request $request,$id)
    {

        $rules = array(
            'judul' => 'required',
            'preview' => 'required',
            'isi' => 'required',
            'gambar' => 'max:2048|mimes:jpeg,jpg,png',
            'categories' => 'required'
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('edit-projects/'.$id)
                  ->withErrors($validator)
                  ->withInput();
        }


        $judul = $request->input('judul');
        $preview = $request->input('preview');
        $tags = $request->input('tags');
        $categories = $request->input('categories');

        $isi = $request->input('isi');
        $id_created = Auth::user()->id;

        if ($tags != "") {
            $n_tags = count($tags);
        }else{
            $n_tags = 0;
        }

        $n_categories = count($categories);

        $lower = strtolower($judul);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $foto = $ganti;
        }else{
            $foto = $lower;
        }

        $data = Blogs::find($id);
        $data->judul = $judul;
        $data->preview = $preview;
        $data->isi = $isi;
        $data->tanggal = Carbon::now();
        $data->id_created = $id_created;

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $file_name = Carbon::now()->timestamp."-".$foto.".". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-blog'); // root storage path
            $file->move($location,$file_name);
            if ($request->input('old_gambar') != "") {
                unlink('storage/app/public/foto-blog/'.$request->input('old_gambar'));
              }
        } else {
            $file_name = $request->input('old_gambar');
        }

        $data->gambar = $file_name;

        if ($data->save()) {

            BlogTags::where('id_blog',$id)->delete();
            BlogCategories::where('id_blog',$id)->delete();

            if ($n_tags > 0) {
                for ($i=0; $i < $n_tags; $i++) {
                    $data_tag = New BlogTags();
                    $data_tag->id_blog = $data->id;
                    $data_tag->id_tags = $tags[$i];
                    $data_tag->save();
                }
            }

            if ($n_categories > 0) {
                for ($i=0; $i < $n_categories; $i++) {
                    $data_categories = New BlogCategories();
                    $data_categories->id_blog = $data->id;
                    $data_categories->id_categories = $categories[$i];
                    $data_categories->save();
                }
            }

            return Redirect::to('data-blog')->with('msg_input','success_edit');
          }else{
            return Redirect::to('data-blog')->with('msg_input','fail_edit');
          }
    }

    public function destroy($id)
    {

      $data = Blogs::find($id);

      if ($data) {

        if ($data->gambar != "") {
            if (file_exists(storage_path('app/public/foto-blog'.$data->gambar))) {
                unlink('storage/app/public/foto-blog/'.$data->gambar);
            }
        }

        BlogTags::where('id_blog',$id)->delete();
        BlogCategories::where('id_blog',$id)->delete();

        $data->delete();

        return Redirect::to('data-blog')->with('msg_input','success_delete');
      }else{
        return Redirect::to('data-blog')->with('msg_input','fail_delete');
      }
    }
}
