<?php

namespace App\Http\Controllers;

use App\BlogCategories;
use App\Clients;
use App\Founder;
use App\Menus;
use App\Messages;
use App\Projects;
use App\Services;
use App\SetApps;
use App\Testimonials;
use App\Blogs;
use App\BlogTags;
use App\Categories;
use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $apps = SetApps::first();

        $menus = Menus::where('status','1')->orderBy('urutan','asc')->get();

        $clients = Clients::where('status','1')->orderBy('urutan','asc')->get();

        $services = Services::where('status','1')->orderBy('id','asc')->get();

        $testimonials = Testimonials::orderBy('id','desc')->get();

        $founder = Founder::orderBy('nama','asc')->get();

        $project = Projects::orderBy('id','desc')->limit(6)->get();

        $blog = Blogs::orderBy('updated_at','desc')->where('publish',1)->limit(4)->get();

        return view('index',compact('apps','menus','clients','services','testimonials','founder','project','blog'));
    }

    public function listBlog()
    {

        $blogs = Blogs::join('data_blog_categories','data_blogs.id','=','data_blog_categories.id_blog','left')
        ->join('data_categories','data_categories.id','=','data_blog_categories.id_categories','left')
        ->join('users','users.id','=','data_blogs.id_created','left')
        ->select('data_blogs.*','data_categories.categories','users.avatar','users.name')
        ->where('publish',1)
        ->orderBy('updated_at','desc')
        ->orderBy('view_count','desc')
        ->paginate(3);

        $categories = BlogCategories::join('data_categories','data_categories.id','=','data_blog_categories.id_categories')->get();
        $tags = BlogTags::join('data_categories','data_categories.id','=','data_blog_tags.id_tags')->get();
        $catg = Categories::get();
        $apps = SetApps::first();


      return view('list_blog',compact('blogs','categories','tags','apps','catg'));

    }

    public function viewBlog($id)
    {

        $apps = SetApps::first();

        $data = Blogs::find($id);
        $data->view_count = $data->view_count+1;
        $data->save();

        $created = User::find($data->id_created);

        $categories = BlogCategories::join('data_categories','data_categories.id','=','data_blog_categories.id_categories','left')
        ->where('id_blog',$id)->get();

        $data_catg = array();
        foreach ($categories as $key => $value) {
            $data_catg[] = $value->id_categories;
        }

        $blog_most = Blogs::join('data_blog_categories','data_blogs.id','=','data_blog_categories.id_blog','left')
                    ->whereIn('data_blog_categories.id_categories',$data_catg)
                    ->where('publish',1)
                    ->orderBy('view_count','desc')
                    ->first();

        $blog_readthis = Blogs::join('data_blog_categories','data_blogs.id','=','data_blog_categories.id_blog','left')
                    ->whereIn('data_blog_categories.id_categories',$data_catg)
                    ->where('publish',1)
                    ->first();

        $blog_related = Blogs::join('data_blog_categories','data_blogs.id','=','data_blog_categories.id_blog','left')
                    ->join('data_categories','data_categories.id','=','data_blog_categories.id_categories','left')
                    ->join('users','users.id','=','data_blogs.id_created','left')
                    ->select('data_blogs.*','data_categories.categories','users.avatar','users.name')
                    ->whereIn('data_blog_categories.id_categories',$data_catg)
                    ->where('data_blogs.id','!=',$id)
                    ->where('publish',1)
                    ->orderBy('view_count','desc')
                    ->limit(3)
                    ->get();

                    // dd($blog_related);

        $tags = BlogTags::join('data_categories','data_categories.id','=','data_blog_tags.id_tags')
        ->where('id_blog',$id)->get();

        return view('view-blog',compact('apps','data','categories','tags','created','blog_most','blog_readthis','blog_related'));
    }

    public function dashboard()
    {


        return view('home');
    }

}
