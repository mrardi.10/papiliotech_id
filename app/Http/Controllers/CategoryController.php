<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    //
    public function index(Request $request)
    {
        $role = Auth::user()->role;
         if ($role == 1){

      if($request->ajax())
      {
        DB::statement(DB::raw('set @rownum=0'));

      $data = DB::table('data_categories')
      ->select('id','categories',DB::raw('@rownum  := @rownum  + 1 AS rownum'))
      ->orderBy('id','desc')
      ->get();

    return Datatables::of($data)
          ->addColumn('action', '<a href="{{ url("edit-category/".$id) }}">
          <button class="btn btn-sm bg-info white" data-toggle="tooltip" data-placement="left" title="Ubah"><i class="fa fa-edit"></i></button>
      </a>
      <a href="{{ url("delete-category/".$id) }}">
          <button class="btn btn-sm bg-danger white" title="hapus"><i class="fa fa-trash"></i></button>
      </a>
      ')
    ->rawColumns(['status','action'])
    ->make(true);
      }

      return view('categories.data');


        }else{
            return abort(404);
        }
    }

    public function create()
    {

      return view('categories.create');
    }

    public function store(Request $request)
    {

        $rules = array(
            'categories' => 'required',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('create-category')
                  ->withErrors($validator)
                  ->withInput();
        }

        $categories = $request->input('categories');

        $data = New Categories();
        $data->categories = $categories;

        if ($data->save()) {
            return Redirect::to('data-category')->with('msg_input','success_create');
          }else{
            return Redirect::to('data-category')->with('msg_input','fail_create');
          }
    }

    public function edit($id)
    {
        $data = Categories::find($id);

      return view('categories.edit',compact('data'));
    }

    public function update(Request $request,$id)
    {

        $rules = array(
            'categories' => 'required',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('edit-category/'.$id)
                  ->withErrors($validator)
                  ->withInput();
        }

        $categories = $request->input('categories');

        $data = Categories::find($id);
        $data->categories = $categories;

        if ($data->save()) {
            return Redirect::to('data-category')->with('msg_input','success_edit');
          }else{
            return Redirect::to('data-category')->with('msg_input','fail_edit');
          }

    }

    public function destroy($id)
    {
        $data = Categories::find($id);
        if ($data) {
            $data->delete();
            return Redirect::to('data-category')->with('msg_input','success_delete');
        }else{
            return Redirect::to('data-category')->with('msg_input','success_delete');
        }



    }
}
