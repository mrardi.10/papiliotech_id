<?php

namespace App\Http\Controllers;

use App\SetApps;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class SettingAppsController extends Controller
{
    //
    public function index(Request $request)
    {
        $role = Auth::user()->role;
         if ($role == 1){

            $apps = SetApps::first();

            if ($apps != "") {
                $data = SetApps::first();
            }else{
                $data = "";
            }

        return view('setting_app.data',compact('data'));


        }else{
            return abort(404);
        }
    }

    public function store(Request $request)
    {

        $rules = array(
            'name' => 'required',
            'version' => 'required',
            'logo' => 'max:2048|mimes:jpeg,jpg,png',
            'email' => 'required',
            'no_hp' => 'required',
            'tentang_kita' => 'required',
            'sejarah' => 'required',
            'alamat' => 'required',
            'logo_footer' => 'mimes:jpeg,jpg,png',
            'logo_header' => 'mimes:jpeg,jpg,png',
            'gambar_tentang' => 'mimes:jpeg,jpg,png',
        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('setting-web')
                  ->withErrors($validator)
                  ->withInput();
        }

        $name = $request->input('name');
        $version = $request->input('version');
        $fb = $request->input('fb');
        $twitter = $request->input('twitter');
        $ig = $request->input('ig');
        $email = $request->input('email');
        $linked_in = $request->input('linked_in');
        $no_hp = $request->input('no_hp');
        $tentang_kita = $request->input('tentang_kita');
        $sejarah = $request->input('sejarah');
        $alamat = $request->input('alamat');
        $meta_keyword = $request->input('meta_keyword');
        $meta_description = $request->input('meta_description');

        if (SetApps::first() != "") {
            $data = SetApps::first();
        }else{
            $data = New SetApps();
        }

        $data->name = $name;
        $data->version = $version;
        $data->fb = $fb;
        $data->twitter = $twitter;
        $data->ig = $ig;
        $data->email = $email;
        $data->linked_in = $linked_in;
        $data->no_hp = $no_hp;
        $data->tentang_kita = $tentang_kita;
        $data->sejarah = $sejarah;
        $data->alamat = $alamat;
        $data->meta_keyword = $meta_keyword;
        $data->meta_description = $meta_description;

        if ($request->hasFile('gambar_tentang')) {
            $file = $request->file('gambar_tentang');
            $gambar_tentang = Carbon::now()->timestamp."-gambar-tentang.". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-web'); // root storage path
            $file->move($location,$gambar_tentang);
            if ($request->input('old_gambar_tentang') != "") {
                unlink('storage/app/public/foto-web/'.$request->input('old_gambar_tentang'));
              }
          } else {

                if ($request->input('old_gambar_tentang') != "") {
                    $gambar_tentang = $request->input('old_gambar_tentang');
                }else{
                    $gambar_tentang = null;
                }

          }
          $data->gambar_tentang = $gambar_tentang;

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $logo = Carbon::now()->timestamp."-logo.". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-web'); // root storage path
            $file->move($location,$logo);
            if ($request->input('old_logo') != "") {
                unlink('storage/app/public/foto-web/'.$request->input('old_logo'));
              }
          } else {
                if ($request->input('old_logo') != "") {
                    $logo = $request->input('old_logo');
                }else{
                    $logo = null;
                }
          }
          $data->logo = $logo;

          if ($request->hasFile('logo_header')) {
            $file = $request->file('logo_header');
            $logo_header = Carbon::now()->timestamp."-logo-header.". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-web'); // root storage path
            $file->move($location,$logo_header);
            if ($request->input('old_logo_header') != "") {
                unlink('storage/app/public/foto-web/'.$request->input('old_logo_header'));
              }

          } else {
                if ($request->input('old_logo_header') != "") {
                    $logo_header = $request->input('old_logo_header');
                }else{
                    $logo_header = null;
                }
          }
          $data->logo_header = $logo_header;

          if ($request->hasFile('logo_footer')) {
            $file = $request->file('logo_footer');
            $logo_footer = Carbon::now()->timestamp."-logo-footer.". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-web'); // root storage path
            $file->move($location,$logo_footer);
            if ($request->input('old_logo_footer') != "") {
                unlink('storage/app/public/foto-web/'.$request->input('old_logo_footer'));
              }
          } else {
                if ($request->input('old_logo_footer') != "") {
                    $logo_footer = $request->input('old_logo_footer');
                }else{
                    $logo_footer = null;
                }
          }
          $data->logo_footer = $logo_footer;

          if ($request->hasFile('logo_sidebar')) {
            $file = $request->file('logo_sidebar');
            $logo_sidebar = Carbon::now()->timestamp."-logo-sidebar.". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-web'); // root storage path
            $file->move($location,$logo_sidebar);
            if ($request->input('old_logo_sidebar') != "") {
                unlink('storage/app/public/foto-web/'.$request->input('old_logo_sidebar'));
              }
          } else {
                if ($request->input('old_logo_sidebar') != "") {
                    $logo_sidebar = $request->input('old_logo_sidebar');
                }else{
                    $logo_sidebar = null;
                }
          }
          $data->logo_sidebar = $logo_sidebar;


          if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $icon = Carbon::now()->timestamp."-icon.". $file->getClientOriginalExtension();
            $location = storage_path('app/public/foto-web'); // root storage path
            $file->move($location,$icon);
            if ($request->input('old_icon') != "") {
                unlink('storage/app/public/foto-web/'.$request->input('old_icon'));
              }
          } else {
                if ($request->input('old_icon') != "") {
                    $icon = $request->input('old_icon');
                }else{
                    $icon = null;
                }
          }
          $data->icon = $icon;



        if ($data->save()) {
            return Redirect::to('setting-web')->with('msg_input','success_create');
          }else{
            return Redirect::to('setting-web')->with('msg_input','fail_create');
          }
    }

}
