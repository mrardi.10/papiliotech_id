<?php

namespace App\Http\Controllers;

use App\Menus;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class MenusController extends Controller
{


    public function index(Request $request)
    {
        $role = Auth::user()->role;
         if ($role == 1){

      if($request->ajax())
      {
        DB::statement(DB::raw('set @rownum=0'));

      $data = DB::table('data_menus')
      ->select('id','nama', 'link','status','urutan',DB::raw('@rownum  := @rownum  + 1 AS rownum'))
      ->orderBy('urutan','asc')
      ->get();

    return Datatables::of($data)
          ->addColumn('status', '@if($status != 0) <span class="label bg-success">Active</span>
          @else
          <span class="label bg-danger">Unctive</span> @endif')
          ->addColumn('action', '<a href="{{ url("edit-menu/".$id) }}">
          <button class="btn btn-sm bg-info white" data-toggle="tooltip" data-placement="left" title="Ubah"><i class="fa fa-edit"></i></button>
      </a>
      <a href="{{ url("change-status/".$id."/menu") }}">
        <button class="btn btn-sm bg-danger white" data-toggle="tooltip" data-placement="left" title="Ubah Status">
            @if ($status == 1)
            <i class="fa fa-toggle-off"></i>
            @else
            <i class="fa fa-toggle-on"></i>
            @endif
        </button>
    </a>')
    ->rawColumns(['status','action'])
    ->make(true);
      }

      return view('menus.data');


        }else{
            return abort(404);
        }
    }

    public function create()
    {

      return view('menus.create');
    }

    public function store(Request $request)
    {

        $rules = array(

            'nama' => 'required',
            'urutan' => 'required',
            'status' => 'required',

        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('create-menu')
                  ->withErrors($validator)
                  ->withInput();
        }

        $nama = $request->input('nama');
        $urutan = $request->input('urutan');
        $status = $request->input('status');

        $lower = strtolower($nama);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $link = $ganti;
        }else{
            $link = $lower;
        }


        $data = New Menus();
        $data->nama = $nama;
        $cek_urutan = Menus::where('urutan',$urutan)->first();
        $last_urutan = Menus::latest('urutan')->first();
        if ($cek_urutan != "") {
            if ($last_urutan != "") {
                $urut = $last_urutan->urutan + 1;
            }else{
                $urut = 1;
            }
            $cek_urutan->urutan = $urut;
            $cek_urutan->save();
        }
        $data->urutan = $urutan;
        $data->link = $link;
        $data->status = $status;

        if ($data->save()) {
            return Redirect::to('data-menu')->with('msg_input','success_create');
          }else{
            return Redirect::to('data-menu')->with('msg_input','fail_create');
          }
    }

    public function edit($id)
    {
        $data = Menus::find($id);

      return view('menus.edit',compact('data'));
    }

    public function update(Request $request,$id)
    {

        $rules = array(

            'nama' => 'required',
            'urutan' => 'required',
            'status' => 'required',

        );

        //VALIDASI INPUTAN
        $validator = Validator::make($request->all(), $rules);
        //PENGECEKAN JIKA GAGAL
        if ($validator->fails()) {
            return Redirect::to('edit-menu/'.$id)
                  ->withErrors($validator)
                  ->withInput();
        }

        $nama = $request->input('nama');
        $urutan = $request->input('urutan');
        $status = $request->input('status');

        $lower = strtolower($nama);
        $ganti = str_replace(' ','-',$lower);
        if ($ganti != "") {
            $link = $ganti;
        }else{
            $link = $lower;
        }


        $data = Menus::find($id);
        $data->nama = $nama;
        $cek_urutan = Menus::where('urutan',$urutan)->first();
        if ($cek_urutan != "") {
            $cek_urutan->urutan = $data->urutan;
            $cek_urutan->save();
        }
        $data->urutan = $urutan;
        $data->link = $link;
        $data->status = $status;

        if ($data->save()) {
            return Redirect::to('data-menu')->with('msg_input','success_edit');
          }else{
            return Redirect::to('data-menu')->with('msg_input','fail_edit');
          }

    }
}
