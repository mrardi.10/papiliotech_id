<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Blogs;
use App\Clients;
use App\Menus;
use App\Messages;
use App\Services;
use App\Testimonials;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard()
    {


        return view('home');
    }

    public function changeStatus($id,$sesi)
    {
        $save = "";
        $url = "";
        $data_name = "";

       if ($sesi == 'menu') {
           $data = Menus::find($id);
           if ($data->status == 1) {
                $status = 0;
           }else{
                $status = 1;
           }
           $data->status = $status;
           $save = $data->save();
           $url = 'data-menu';
           $data_name = $data->nama;

    }elseif ($sesi == 'services') {
        $data = Services::find($id);
        if ($data->status == 1) {
             $status = 0;
        }else{
             $status = 1;
        }
        $data->status = $status;
        $save = $data->save();
        $url = 'data-services';
        $data_name = $data->service;

    }elseif ($sesi == 'clients') {
        $data = Clients::find($id);
        if ($data->status == 1) {
             $status = 0;
        }else{
             $status = 1;
        }
        $data->status = $status;
        $save = $data->save();
        $url = 'data-clients';
        $data_name = $data->client;

    }elseif ($sesi == 'messages') {
        $data = Messages::find($id);
        if ($data->status == 1) {
             $status = 0;
        }else{
             $status = 1;
        }
        $data->status = $status;
        $save = $data->save();
        $url = 'data-messages';
        $data_name = $data->full_name;

    }elseif ($sesi == 'testimonial') {
        $data = Testimonials::find($id);
        if ($data->status == 1) {
             $status = 0;
        }else{
             $status = 1;
        }
        $data->status = $status;
        $save = $data->save();
        $url = 'data-testimonials';
        $data_name = $data->nama;
    }elseif ($sesi == 'blog') {
        $data = Blogs::find($id);
        if ($data->publish == 1) {
             $status = 0;
        }else{
             $status = 1;
        }
        $data->publish = $status;
        $save = $data->save();
        $url = 'data-blog';
        $data_name = $data->judul;

    }elseif ($sesi == 'administrator') {
        $data = Admin::find($id);
        if ($data->status == 1) {
             $status = 0;
        }else{
             $status = 1;
        }
        $data->status = $status;
        $save = $data->save();
        $url = 'data-administrator';
        $data_name = $data->name;

    }else{

       }
             if ($save != "") {
               return Redirect::to($url)->with('msg_input','success_change_status')->with('data',$data_name);
             }else{
               return Redirect::to($url)->with('msg_input','fail_change_status')->with('data',$data_name);
             }
     }

     public function FileView($path,$file)
    {
        $pathToFile = storage_path('app/public'."/".$path."/".$file);
        return response()->file($pathToFile);
    }
}
