<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTags extends Model
{
    //
    protected $table = 'data_blog_tags';
}
